const path = require('path');
const util = require('util');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Bootcamp = require('../models/Bootcamp');
const City = require('../models/City');
const User = require('../models/User');

exports.getBootcamps = asyncHandler(async (req, res, next) => {
  const data = [];
  for (const item of res.advancedResults.data) {
    let fromLocation = await City.findById(item.fromLocation);
    let toLocation = await City.findById(item.toLocation);
    let userModal = await User.findById(item.user);
    data.push({
      ...item.toObject(),
      userModal,
      fromLocation: {
        id: item.fromLocation,
        name: fromLocation.name
      },
      toLocation: {
        id: item.toLocation,
        name: toLocation.name
      }
    })
  }
  res.advancedResults.data = data;
  res.status(200).json(res.advancedResults);
});

exports.getMyBootcamps = asyncHandler(async (req, res, next) => {
  const data = [];
  for (const item of res.advancedResults.data) {
    let fromLocation = await City.findById(item.fromLocation);
    let toLocation = await City.findById(item.toLocation);
    let userModal = await User.findById(item.user);
    if (req.params.id === `${userModal._id}`) {
      data.push({
        ...item.toObject(),
        userModal,
        fromLocation: {
          id: item.fromLocation,
          name: fromLocation.name
        },
        toLocation: {
          id: item.toLocation,
          name: toLocation.name
        }
      });
    }
  }
  res.advancedResults.data = data;
  res.status(200).json(res.advancedResults);
});

exports.getBootcamp = asyncHandler(async (req, res, next) => {
  let bootcamp = await Bootcamp.findById(req.params.id);
  const fromLocation = await City.findById(bootcamp.fromLocation);
  const toLocation = await City.findById(bootcamp.toLocation);
  let userModal = await User.findById(bootcamp.user);

  bootcamp = {
    ...bootcamp.toObject(),
    userModal,
    fromLocation: {
      id: bootcamp.fromLocation,
      name: fromLocation.name
    },
    toLocation: {
      id: bootcamp.toLocation,
      name: toLocation.name
    },
  };

  if (!bootcamp) {
    return next(
      new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
    );
  }

  res.status(200).json({ success: true, data: bootcamp });
});

// @desc      Create new bootcamp
// @route     POST /api/v1/bootcamps
// @access    Private
exports.createBootcamp = asyncHandler(async (req, res, next) => {
  // Add user to req,body
  req.body.user = req.user.id;

  const bootcamp = await Bootcamp.create({...req.body, fromLocation: req.body.fromLocationId, toLocation: req.body.toLocationId});

  res.status(201).json({
    success: true,
    data: bootcamp
  });
});

// @desc      Update bootcamp
// @route     PUT /api/v1/bootcamps/:id
// @access    Private
exports.updateBootcamp = asyncHandler(async (req, res, next) => {
  let bootcamp = await Bootcamp.findById(req.params.id);

  if (!bootcamp) {
    return next(
      new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is bootcamp owner
  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update this bootcamp`,
        401
      )
    );
  }

  bootcamp = await Bootcamp.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true
  });

  res.status(200).json({ success: true, data: bootcamp });
});

// @desc      Delete bootcamp
// @route     DELETE /api/v1/bootcamps/:id
// @access    Private
exports.deleteBootcamp = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  if (!bootcamp) {
    return next(
      new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is bootcamp owner
  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to delete this bootcamp`,
        401
      )
    );
  }

  await bootcamp.remove();

  res.status(200).json({ success: true, data: {} });
});

// @desc      Get bootcamps within a radius
// @route     GET /api/v1/bootcamps/radius/:zipcode/:distance
// @access    Private
exports.getBootcampsInRadius = asyncHandler(async (req, res, next) => {
  const { zipcode, distance } = req.params;
  res.status(200).json({
    success: true,
    count: bootcamps.length,
    data: bootcamps
  });
});

// @desc      Upload photo for bootcamp
// @route     PUT /api/v1/bootcamps/:id/photo
// @access    Private
exports.bootcampPhotoUpload = asyncHandler(async (req, res, next) => {
  const bootcamp = await Bootcamp.findById(req.params.id);

  if (!bootcamp) {
    return next(
      new ErrorResponse(`Bootcamp not found with id of ${req.params.id}`, 404)
    );
  }

  // Make sure user is bootcamp owner
  if (bootcamp.user.toString() !== req.user.id && req.user.role !== 'admin') {
    return next(
      new ErrorResponse(
        `User ${req.user.id} is not authorized to update this bootcamp`,
        401
      )
    );
  }

  if (!req.files) {
    return next(new ErrorResponse(`Please upload a file`, 400));
  }

  const file = req.files.file;
  if (file.length) {
    const fileNames = [];
    for (let i = 0; i < file.length; i++) {
      const item = file[i];
      // Make sure the image is a photo
      if (!item.mimetype.startsWith('image')) {
        return next(new ErrorResponse(`Please upload an image file`, 400));
      }
      // Check filesize
      if (item.size > process.env.MAX_FILE_UPLOAD) {
        return next(
          new ErrorResponse(
            `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
            400
          )
        );
      }
      item.name = `photo_${bootcamp._id}${path.parse(item.name).ext}`;
      // Create custom filename
      item.mv(`${process.env.FILE_UPLOAD_PATH}/${item.name}`, async err => {
        if (err) {
          return next(new ErrorResponse(`Problem with file upload`, 500));
        }
        console.log(item.name);
        fileNames.push(item.name);
        await Bootcamp.findByIdAndUpdate(req.params.id, { photo: [...fileNames, ...bootcamp.photo] });
      });
    }

    res.status(200).json({
      success: true,
      data: fileNames
    });
  } else {
    // Make sure the image is a photo
    if (!file.mimetype.startsWith('image')) {
      return next(new ErrorResponse(`Please upload an image file`, 400));
    }
    // Check filesize
    if (file.size > process.env.MAX_FILE_UPLOAD) {
      return next(
        new ErrorResponse(
          `Please upload an image less than ${process.env.MAX_FILE_UPLOAD}`,
          400
        )
      );
    }
    // Create custom filename
    file.name = `photo_${bootcamp._id}${path.parse(file.name).ext}`;
    file.mv(`${process.env.FILE_UPLOAD_PATH}/${file.name}`, async err => {
      if (err) {
        console.error(err);
        return next(new ErrorResponse(`Problem with file upload`, 500));
      }

      await Bootcamp.findByIdAndUpdate(req.params.id, { photo: [file.name] });

      res.status(200).json({
        success: true,
        data: [file.name]
      });
    });
  }
});
