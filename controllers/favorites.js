const path = require('path');
const util = require('util');
const ErrorResponse = require('../utils/errorResponse');
const asyncHandler = require('../middleware/async');
const Favorite = require('../models/Favorite');
const Bootcamp = require('../models/Bootcamp');

exports.getFavorites = asyncHandler(async (req, res, next) => {
  res.status(200).json(res.advancedResults);
});

exports.createFavorite = asyncHandler(async (req, res, next) => {
  const favorite = await Favorite.create(req.body);
  await Bootcamp.findByIdAndUpdate(req.body.bootcampId, {inFavorite: true, favoriteId: favorite._id });
  res.status(201).json({
    success: true,
    data: favorite
  });
});

exports.deleteFavorite = asyncHandler(async (req, res, next) => {
  const fovorite = await Favorite.findById(req.params.id);

  if (!fovorite) {
    return next(
      new ErrorResponse(`Favorite not found with id of ${req.params.id}`, 404)
    );
  }
  await Bootcamp.findByIdAndUpdate(fovorite.bootcampId, {inFavorite: false, favoriteId: '' });
  await fovorite.remove();

  res.status(200).json({success: true, data: {}});
});
