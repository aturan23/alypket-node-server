const puppeteer = require('puppeteer');
const asyncHandler = require('../middleware/async');

exports.loadPage = asyncHandler(async (req, res, next) => {
  const url = req.url.substr(5);
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);
  const body = await page.content();

  await browser.close();
  res.set('Content-Type', 'text/html');

  res.status(200).send(body);
});
