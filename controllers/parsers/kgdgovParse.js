const puppeteer = require('puppeteer');
const fs = require('fs');
const captcha = require("async-captcha");
const anticaptcha = new captcha('0b0667ad32f1dc0420a1f6ed55d411b6', 2, 10);
const HTMLParser = require('node-html-parser');

const kgdgovBodyParse = async (bin) => {
  const url = 'http://kgd.gov.kz/ru/app/culs-taxarrear-search-web';
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);
  await page.screenshot({path: 'captcha.png', clip: {x: 50, y: 500, width: 300, height: 100}});
  const captchaCode = await anticaptcha.getResult(base64_encode('captcha.png'));
  await page.type('#iinBin', bin);
  await page.type('#captcha-user-value', captchaCode.toUpperCase());
  await page.click('#btnGetResult');
  await delay(5000);
  const body = await page.content();
  await browser.close();
  return parser(body);
};

const parser = (body) => {
  const root = HTMLParser.parse(body);
  const mainList = [];
  const infoTitleList = []
  const infoValueList = [];
  root.querySelectorAll('#table0 td').map((item, index) => {
    if (index % 2 === 0) {
      infoTitleList.push(item.innerHTML);
    } else {
      infoValueList.push(item.innerHTML);
    }
  });
  for (let i = 0; i < infoTitleList.length; i++) {
    mainList.push({
      title: infoTitleList[i],
      value: infoValueList[i],
    });
  }
  const elements = root.querySelectorAll('.panel.panel-default').map((element) => {
    const title = element.querySelector('.panel-heading.font-bold');
    if (title) {
      const subTitleList = element.querySelectorAll('.panel-body div').map((item) => item.innerHTML);
      const tableOne = element.querySelectorAll('.table.table-bordered.table-hover')[0].querySelectorAll('tr').map((item) => {
        const td = item.querySelectorAll('td');
        if (td.length > 0) {
          return { title: td[0].innerHTML, value: td[1].innerHTML};
        }
      });
      const secondaryTable = element.querySelectorAll('.wrapper.m-t').map((item) => {
        return item.querySelectorAll('.table.table-bordered.table-hover')[0].querySelectorAll('tr').map((item) => {
          const td = item.querySelectorAll('td');
          if (td.length > 0) {
            return {title: td[0].innerHTML, value: td[1].innerHTML};
          }
        });
      });
      const tableTwo = [].concat(...secondaryTable);
      return {
        title: title.innerHTML,
        subTitleList,
        tables: [...tableOne, ...tableTwo],
      }
    }
  });
  return {
    mainList,
    elements: elements.filter((item) => !!item),
  };
}

const base64_encode = (file) => {
  const bitmap = fs.readFileSync(file);
  return new Buffer(bitmap).toString('base64');
}

const delay = (time) => {
  return new Promise((resolve) => {
    setTimeout(resolve, time)
  });
}

module.exports = kgdgovBodyParse;
