const site_str =
	'<!DOCTYPE html><html class="no-js js" xml:lang="ru" lang="ru" dir="ltr" xmlns:content="http://purl.org/rss/1.0/modules/content/" xmlns:dc="http://purl.org/dc/terms/" xmlns:foaf="http://xmlns.com/foaf/0.1/" xmlns:og="http://ogp.me/ns#" xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#" xmlns:sioc="http://rdfs.org/sioc/ns#" xmlns:sioct="http://rdfs.org/sioc/types#" xmlns:skos="http://www.w3.org/2004/02/skos/core#" xmlns:xsd="http://www.w3.org/2001/XMLSchema#"><!--<![endif]--><head>\n' +
	'        <!--[if IE]><![endif]-->\n' +
	'<meta charset="utf-8">\n' +
	'<meta property="og:image:width" content="300">\n' +
	'<meta property="og:image:height" content="300">\n' +
	'<meta property="og:image" content="http://kgd.gov.kz/sites/all/themes/KGD17/images/logo_social.jpg">\n' +
	'<meta property="og:description" content="">\n' +
	'<meta name="Generator" content="Drupal 7 (http://drupal.org)">\n' +
	'<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">\n' +
	'<meta property="og:title" content="Сведения об отсутствии (наличии) задолженности, учет по которым ведется в органах государственных доходов | Комитет государственных доходов Министерства финансов Республики Казахстан">\n' +
	'    <title>Сведения об отсутствии (наличии) задолженности, учет по которым ведется в органах государственных доходов | Комитет государственных доходов Министерства финансов Республики Казахстан</title>\n' +
	'    <style type="text/css" media="all">\n' +
	'@import url("//kgd.gov.kz/modules/system/system.base.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/system/system.menus.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/system/system.messages.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/system/system.theme.css?q9wjlm");\n' +
	'</style>\n' +
	'<style type="text/css" media="all">\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/colorbox_node/colorbox_node.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/comment/comment.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/field/theme/field.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/node/node.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/search/search.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/user/user.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/forum/forum.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/views/css/views.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/ckeditor/css/ckeditor.css?q9wjlm");\n' +
	'</style>\n' +
	'<style type="text/css" media="all">\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/footable.core.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/cctags/cctags.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/colorbox/styles/default/colorbox_style.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/ctools/css/ctools.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/modules/payment_forms/payment_forms.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/modules/locale/locale.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/libraries/smartmenus/css/sm-core-css.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/libraries/smartmenus/css/sm-kgd/sm-kgd.css?q9wjlm");\n' +
	'</style>\n' +
	'<style type="text/css" media="all">\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/bootstrap.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/animate.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/font-awesome.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/simple-line-icons.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/app.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/flexslider.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/datepicker.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/kgd17.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/owl.carousel.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/owl.theme.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/special.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/jquery.navgoco.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/css/jquery.dataTables.css?q9wjlm");\n' +
	'@import url("//kgd.gov.kz/sites/all/themes/KGD17/style.css?q9wjlm");\n' +
	'</style>\n' +
	'    <script type="text/javascript" async="" src="http://c.zero.kz/z.js"></script><script async="" type="text/javascript" src="http://openstat.net/cnt.js"></script><script type="text/javascript" async="" src="https://mc.yandex.ru/metrika/watch.js"></script><script type="text/javascript" src="//kgd.gov.kz/sites/default/files/advagg_js/js__AXMOlXxASvSCyClBMqWGzsKDyv18_Xuo7Ly9eTwu6sw__iMDTHbn5QT3iV4qEetklREdCJAoGQO9-0RO1h8naYcA__iFQX4H3jw4qSn9BeERFJlBuZNBASR3Or1rI0CUFuY7Q.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/jquery-ui.min.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/loadingoverlay.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/jquery.localize.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/jquery.validate.min.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/datepicker.locales.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/app.js"></script>\n' +
	'<script type="text/javascript" src="/apps/services/StaticResourcesWeb/js/app-translations.js"></script>\n' +
	'<script type="text/javascript" src="//kgd.gov.kz/sites/default/files/advagg_js/js__wpxK_IceezmrMCdNfdZx3t6n2igWeP8yG2xwwmiljJM__ucYwshErub6FONqTxKldxWvw367A2Y3QppDtueIu4s4__iFQX4H3jw4qSn9BeERFJlBuZNBASR3Or1rI0CUFuY7Q.js"></script>\n' +
	'<script type="text/javascript" src="//kgd.gov.kz/sites/default/files/advagg_js/js__0BLiKGMq6ltLHdFnULA09l4oeJRFJqujRKfo_ka1H2M__VZq7dwkTjN_yzboliiXzKkblH3SFDmh0AVSmuD6eiTw__iFQX4H3jw4qSn9BeERFJlBuZNBASR3Or1rI0CUFuY7Q.js"></script>\n' +
	'<script type="text/javascript" src="//kgd.gov.kz/sites/default/files/advagg_js/js__kgBzEew375opErticaz0339aAXuAnDgK4WnFDvdZJTQ__q2RlwAY16ZoDPR8qo8jEDHi9AlWhUuqwDMfKJJ7KwO4__iFQX4H3jw4qSn9BeERFJlBuZNBASR3Or1rI0CUFuY7Q.js"></script>\n' +
	'<script type="text/javascript">\n' +
	'<!--//--><![CDATA[//><!--\n' +
	'jQuery.extend(Drupal.settings,{"basePath":"\\/","pathPrefix":"ru\\/","ajaxPageState":{"theme":"KGD17","theme_token":"o1aeGuh5shtxR0qkfiKvomNKFMezzOmjrEQA_oXarlw","jquery_version":"1.10","css":{"\\/apps\\/services\\/StaticResourcesWeb\\/css\\/animate.css":1,"\\/apps\\/services\\/StaticResourcesWeb\\/css\\/app.css":1,"\\/apps\\/services\\/StaticResourcesWeb\\/css\\/simple-line-icons.css":1,"modules\\/system\\/system.base.css":1,"modules\\/system\\/system.menus.css":1,"modules\\/system\\/system.messages.css":1,"modules\\/system\\/system.theme.css":1,"sites\\/all\\/modules\\/colorbox_node\\/colorbox_node.css":1,"modules\\/comment\\/comment.css":1,"modules\\/field\\/theme\\/field.css":1,"modules\\/node\\/node.css":1,"modules\\/search\\/search.css":1,"modules\\/user\\/user.css":1,"modules\\/forum\\/forum.css":1,"sites\\/all\\/modules\\/views\\/css\\/views.css":1,"sites\\/all\\/modules\\/ckeditor\\/css\\/ckeditor.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/footable.core.css":1,"sites\\/all\\/modules\\/cctags\\/cctags.css":1,"sites\\/all\\/modules\\/colorbox\\/styles\\/default\\/colorbox_style.css":1,"sites\\/all\\/modules\\/ctools\\/css\\/ctools.css":1,"sites\\/all\\/modules\\/payment_forms\\/payment_forms.css":1,"modules\\/locale\\/locale.css":1,"sites\\/all\\/libraries\\/smartmenus\\/css\\/sm-core-css.css":1,"sites\\/all\\/libraries\\/smartmenus\\/css\\/sm-kgd\\/sm-kgd.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/bootstrap.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/animate.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/font-awesome.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/simple-line-icons.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/app.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/flexslider.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/datepicker.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/kgd17.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/owl.carousel.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/owl.theme.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/special.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/jquery.navgoco.css":1,"sites\\/all\\/themes\\/KGD17\\/css\\/jquery.dataTables.css":1,"sites\\/all\\/themes\\/KGD17\\/style.css":1},"js":{"sites\\/all\\/libraries\\/smartmenus\\/jquery.smartmenus.js":1,"sites\\/all\\/modules\\/jquery_update\\/replace\\/jquery\\/1.10\\/jquery.min.js":1,"misc\\/jquery.once.js":1,"misc\\/drupal.js":1,"sites\\/all\\/modules\\/jquery_update\\/replace\\/ui\\/external\\/jquery.cookie.js":1,"misc\\/ajax.js":1,"sites\\/all\\/modules\\/jquery_update\\/js\\/jquery_update.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/footable.js":1,"public:\\/\\/languages\\/ru_N2amcfHewUshjKMi3JkDPbRHfhKXtokccCgDrCkm0ig.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/forum_for_mobile.js":1,"sites\\/all\\/libraries\\/colorbox\\/jquery.colorbox-min.js":1,"sites\\/all\\/modules\\/colorbox\\/js\\/colorbox.js":1,"sites\\/all\\/modules\\/colorbox\\/styles\\/default\\/colorbox_style.js":1,"sites\\/all\\/modules\\/colorbox\\/js\\/colorbox_load.js":1,"sites\\/all\\/modules\\/colorbox\\/js\\/colorbox_inline.js":1,"sites\\/all\\/modules\\/app_srv\\/js\\/j.app.start.js":1,"sites\\/all\\/modules\\/cctags\\/js\\/jquery.tagcanvas.js":1,"sites\\/all\\/modules\\/cctags\\/js\\/j.tag.start.js":1,"sites\\/all\\/modules\\/smartmenus\\/js\\/smartmenus.settings.js":1,"misc\\/progress.js":1,"sites\\/all\\/modules\\/colorbox_node\\/colorbox_node.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/jquery-ui.min.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/loadingoverlay.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/jquery.localize.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/jquery.validate.min.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/datepicker.locales.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/app.js":1,"\\/apps\\/services\\/StaticResourcesWeb\\/js\\/app-translations.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/bootstrap.min.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/jquery-ui.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/superfish.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/mobilemenu.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/owl.carousel.min.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/owl.carousel.start.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/custom.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/jquery.navgoco.min.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/ui.datepicker-lang.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/jquery.scrolltofixed.min.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/specver.js":1,"sites\\/all\\/themes\\/KGD17\\/js\\/functions.js":1}},"colorbox":{"opacity":"0.85","current":"{current} \u0438\u0437 {total}","previous":"\u00ab \u041f\u0440\u0435\u0434\u044b\u0434\u0443\u0449\u0438\u0439","next":"\u0421\u043b\u0435\u0434\u0443\u044e\u0449\u0438\u0439 \u00bb","close":"\u0417\u0430\u043a\u0440\u044b\u0442\u044c","maxWidth":"98%","maxHeight":"98%","fixed":true,"mobiledetect":true,"mobiledevicewidth":"480px"},"app":"culs-taxarrear-search-web","locale":"ru","app_ver":"0.1.2","mod_ver":"7.x-1.0","authorized":false,"smartmenus":{"smartmenus_2":{"options":{"subMenusSubOffsetX":1,"subMenusSubOffsetY":-8,"showOnClick":true,"hideOnClick":true,"noMouseOver":true},"settings":{"activeTrail":true}},"smartmenus_1":{"options":{"subMenusSubOffsetX":1,"subMenusSubOffsetY":-8,"showOnClick":true,"hideOnClick":true,"noMouseOver":true},"settings":{"activeTrail":true}}},"urlIsAjaxTrusted":{"\\/ru\\/app\\/culs-taxarrear-search-web?destination=app\\/culs-taxarrear-search-web":true},"colorbox_node":{"width":"600px","height":"600px"}});\n' +
	'//--><!]]>\n' +
	'</script>\n' +
	'    <link href="http://kgd.gov.kz/favicon.ico?new" rel="icon" type="image/ico">\n' +
	"    <!--[if !IE]><!--><script>if (/*@cc_on!@*/false) { document.documentElement.className = 'ie ie10'; }</script><!--<![endif]-->\n" +
	'    \n' +
	'  </head>\n' +
	'  <body class="html not-front not-logged-in one-sidebar sidebar-first page-app page-app-culs-taxarrear-search-web domain-kgd-gov-kz i18n-ru normal c1 font-small imageson" style="">\n' +
	'    <div id="body_h">\n' +
	'      <!--[if lt IE 7]>\n' +
	'        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>\n' +
	'      <![endif]-->\n' +
	'      <!-- <div id="skip">\n' +
	'        <a href="#main-menu">Перейти к навигации</a>\n' +
	'      </div> -->\n' +
	'            \n' +
	'  <a href="/ru/apoll"><div class="voteTop"><span>Опрос</span></div></a>\n' +
	'<div id="page">\n' +
	'  <div id="CecutientWrapper" style="display: none;">\n' +
	'    <div id="panelGost" class="wrapper-sm text-center text-md access">\n' +
	'      <div class="acolors outer">\n' +
	'        <div class="clearfix">\n' +
	'          <dl class="a-colors">\n' +
	'            <dt>Цвет сайта</dt>\n' +
	'            <dd class="a-c1"><a rel="c1" id="WhiteStyle" class="active">Чёрным по белому</a></dd>\n' +
	'            <dd class="a-c2"><a rel="c2" id="BlackStyle">Белым по чёрному</a></dd>\n' +
	'            <dd class="a-c3"><a rel="c3" id="BlueStyle">Тёмно-синим по голубому</a></dd>\n' +
	'          </dl>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'      <div class="aimages outer">\n' +
	'        <div class="clearfix">\n' +
	'          <dl class="a-images">\n' +
	'            <dt>Изображения</dt>\n' +
	'            <dd>\n' +
	'              <a rel="imagesoff" id="ImageOn" class="a-imagesoff active" title="Switch images off/on">Switch images off/on</a>\n' +
	'              <a rel="imagesoff" id="ImageOff" class="a-imagesoff active" title="Switch images off/on">Switch images off/on</a>\n' +
	'            </dd>\n' +
	'          </dl>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'      <div class="asettings outer">\n' +
	'        <div class="clearfix">\n' +
	'          <dl class="a-settings">\n' +
	'            <dt>Настройки</dt>\n' +
	'            <dd><a id="settingVer" title="Настройки">Настройки</a></dd>\n' +
	'          </dl>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'      <div class="asimple outer last">\n' +
	'        <div class="clearfix">\n' +
	'          <dl class="a-simple">\n' +
	'            <dt>Обычная версия</dt>\n' +
	'            <dd><a id="NormalVer" class="default" title="Обычная версия">Обычная версия</a></dd>\n' +
	'          </dl>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'    <div id="poppedSettings">\n' +
	'      <div class="block-choose-color">\n' +
	'        <p class="choose-color-title">Цветовая схема:</p>\n' +
	'        <ul class="a-colors choose-colors">\n' +
	'          <li id="c1" class="a-c1"><a rel="c1" id="WhiteStyle2" title="Чёрным по белому" class="active">Чёрным по белому</a></li>\n' +
	'          <li id="c2" class="a-c2"><a rel="c2" id="BlackStyle2" title="Белым по чёрному">Белым по чёрному</a></li>\n' +
	'          <li id="c3" class="a-c3"><a rel="c3" id="BlueStyle2" title="Тёмно-синим по голубому">Тёмно-синим по голубому</a></li>\n' +
	'          <li id="c4" class="a-c4"><a rel="c4" id="BrownStyle" title="Коричневым по бежевому">Коричневым по бежевому</a></li>\n' +
	'          <li id="c5" class="a-c5"><a rel="c5" id="GreenStyle" title="Зеленым по темно-коричневому">Зеленым по темно-коричневому</a></li>\n' +
	'        </ul>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'\n' +
	'  <div id="headUp">\n' +
	'    <div class="container">\n' +
	'      <div class="row hidden-md hidden-lg">\n' +
	'        <div class="col-xs-12 no-padder mobilemenu">\n' +
	'          <ul id="mobileLang" class="pagination pagination-sm pagination-lang centering">\n' +
	'                                        <li class="kk">\n' +
	'                <a href="/kk/app/culs-taxarrear-search-web" style="margin-left: 321px;">\n' +
	'                  Қаз                </a>\n' +
	'              </li>\n' +
	'                                        <li class="ru active">\n' +
	'                <a href="/ru/app/culs-taxarrear-search-web">\n' +
	'                  Рус                </a>\n' +
	'              </li>\n' +
	'                                        <li class="en">\n' +
	'                <a href="/en/app/culs-taxarrear-search-web">\n' +
	'                  Eng                </a>\n' +
	'              </li>\n' +
	'                      </ul>\n' +
	'          <ul class="pagination pagination-sm pagination-lang">\n' +
	'            <li class="menu pull-right">\n' +
	'              <a class="dMenu">\n' +
	'                <i class="fa fa-caret-down fa-lg"></i>\n' +
	'              </a>\n' +
	'            </li>\n' +
	'          </ul>\n' +
	'        </div>\n' +
	'        <div id="caretDown" class="col-sm-12">\n' +
	'          <div class="m-t-xs"></div>\n' +
	'          <div class="text-center">\n' +
	'            <a href="/ru/sitemap" class="btn btn-top btn-xs lineh"><i class="fa fa-sitemap fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://play.google.com/store/apps/details?id=kz.gov.kgd.android" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-android fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://itunes.apple.com/us/app/kgd-mf-rk/id1107556666?mt=8" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-apple fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://www.facebook.com/kgd.gov.kz/?fref=ts" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-facebook fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://twitter.com/kgd_mf_rk" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-twitter fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://plus.google.com/u/0/117682662040777385365/posts" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-google-plus fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://www.youtube.com/user/salykgovkz" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-youtube-play fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'            <a href="https://www.instagram.com/kgd_mf_rk/?hl=ru" class="btn btn-top btn-xs" target="_blank"><i class="fa fa-instagram fa-lg m-l-xs m-r-xs"></i></a>\n' +
	'          </div>\n' +
	'          <div class="split"></div>\n' +
	'          <div id="loginBtn" class="w-full small">\n' +
	'                          <div class="text-center"><a class="log" id="loginMobile" data-id-dialog="loginFormDialog">Вход</a> / <a class="log" href="/ru/user/register">Регистрация</a></div>\n' +
	'                      </div>\n' +
	'          <div class="split"></div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'      <div id="mobiledepmenu" class="col-sm-12 hidden-md hidden-lg no-padder text-center"></div>\n' +
	'      <div class="row m-t-sm hidden-xs hidden-sm">\n' +
	'        <div id="depmenu" class="pull-left">\n' +
	'          <div class="w-full">\n' +
	'                          <div class="region region-top-left">\n' +
	'  <div id="block-smartmenus-smartmenus-2" class="block block-smartmenus">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <ul id="smartmenus_2" class="sm sm-kgd" data-smartmenus-id="15891105248605896"><li class="first last"><a title="" class="nolink has-submenu" id="sm-15891105248605896-1" aria-haspopup="true" aria-controls="sm-15891105248605896-2" aria-expanded="false"><span class="sub-arrow">+</span>Департаменты по регионам</a><ul class="menu" id="sm-15891105248605896-2" role="group" aria-hidden="true" aria-labelledby="sm-15891105248605896-1" aria-expanded="false"><li class="first"><a href="http://nursultan.kgd.gov.kz" title="">Нур-Султан</a></li>\n' +
	'<li class=""><a href="http://almaty.kgd.gov.kz" title="">Алматы</a></li>\n' +
	'<li class=""><a href="http://shymkent.kgd.gov.kz" title="">Шымкент</a></li>\n' +
	'<li class=""><a href="http://akm.kgd.gov.kz" title="">Акмолинская область</a></li>\n' +
	'<li class=""><a href="http://akb.kgd.gov.kz" title="">Актюбинская область</a></li>\n' +
	'<li class=""><a href="http://alm.kgd.gov.kz" title="">Алматинская область</a></li>\n' +
	'<li class=""><a href="http://atr.kgd.gov.kz" title="">Атырауская область</a></li>\n' +
	'<li class=""><a href="http://vko.kgd.gov.kz" title="">Восточно-Казахстанская область</a></li>\n' +
	'<li class=""><a href="http://zhmb.kgd.gov.kz" title="">Жамбылская область</a></li>\n' +
	'<li class=""><a href="http://zko.kgd.gov.kz" title="">Западно-Казахстанская область</a></li>\n' +
	'<li class=""><a href="http://krg.kgd.gov.kz" title="">Карагандинская область</a></li>\n' +
	'<li class=""><a href="http://kst.kgd.gov.kz" title="">Костанайская область</a></li>\n' +
	'<li class=""><a href="http://kzl.kgd.gov.kz" title="">Кызылординская область</a></li>\n' +
	'<li class=""><a href="http://mng.kgd.gov.kz" title="">Мангистауская область</a></li>\n' +
	'<li class=""><a href="http://pvl.kgd.gov.kz" title="">Павлодарская область</a></li>\n' +
	'<li class=""><a href="http://sko.kgd.gov.kz" title="">Северо-Казахстанская область</a></li>\n' +
	'<li class="last"><a href="http://trk.kgd.gov.kz" title="">Туркестанская область</a></li>\n' +
	'</ul></li>\n' +
	'</ul>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'                                  </div>\n' +
	'        </div>\n' +
	'        <div id="centerUp" class="inline-block hidden-xs hidden-sm padder-l-lg">\n' +
	'          <div class="pull-left b-c-boss b-r set-font-size">\n' +
	'            <a id="eye-font-small" class="text-xs m-r-sm active" rel="font-small">A</a>\n' +
	'            <a id="eye-font-normal" class="text-base m-r-sm" rel="font-normal">A</a>\n' +
	'            <a id="eye-font-big" class="text-lg m-r-sm" rel="font-big">A</a>\n' +
	'          </div>\n' +
	'          <a href="#" id="enableuGost" class="btn btn-top btn-xs pull-left lineh"><i class="fa fa-eye fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="/ru/sitemap" class="btn btn-top btn-xs pull-left lineh"><i class="fa fa-sitemap fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://play.google.com/store/apps/details?id=kz.gov.kgd.android" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-android fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://itunes.apple.com/us/app/kgd-mf-rk/id1107556666?mt=8" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-apple fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://www.facebook.com/kgd.gov.kz/?fref=ts" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-facebook fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://twitter.com/kgd_mf_rk" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-twitter fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://plus.google.com/u/0/117682662040777385365/posts" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-google-plus fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://www.youtube.com/user/salykgovkz" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-youtube-play fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'          <a href="https://www.instagram.com/kgd_mf_rk/?hl=ru" class="btn btn-top btn-xs pull-left lineh" target="_blank"><i class="fa fa-instagram fa-lg m-l-sm m-r-sm"></i></a>\n' +
	'\n' +
	'            <ul class="pagination pagination-lang pull-left">\n' +
	'                                              <li class="kk">\n' +
	'                  <a href="/kk/app/culs-taxarrear-search-web">\n' +
	'                    Қаз                  </a>\n' +
	'                </li>\n' +
	'                                              <li class="ru active">\n' +
	'                  <a href="/ru/app/culs-taxarrear-search-web">\n' +
	'                    Рус                  </a>\n' +
	'                </li>\n' +
	'                                              <li class="en">\n' +
	'                  <a href="/en/app/culs-taxarrear-search-web">\n' +
	'                    Eng                  </a>\n' +
	'                </li>\n' +
	'                          </ul>\n' +
	'        </div>\n' +
	'        <div id="loginBtn" class="pull-right small">\n' +
	'                          <div class="m-t-xs"><a class="log" id="login" data-id-dialog="loginFormDialog">Вход</a> / <a class="log" href="/ru/user/register">Регистрация</a></div>\n' +
	'                      </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'\n' +
	'  <header id="masthead" class="site-header container" role="banner">\n' +
	'    <div class="row">\n' +
	'      <div class="pull-left header">\n' +
	'        <h2 id="site-name">\n' +
	'          <a class="kgd_logo" href="/ru" title="Комитет государственных доходов Министерства финансов Республики Казахстан"></a>\n' +
	'          <a class="m-l m-r" href="/ru" title="Главная" rel="home"><span><strong>Комитет Государственных Доходов</strong> Министерства финансов Республики Казахстан</span><div class="suffix">Официальный интернет-ресурс</div></a>\n' +
	'        </h2>\n' +
	'      </div>\n' +
	'      <div id="widgetNum" class="col-md-5 col-xs-12 col-sm-12 m-t-lg pull-right">\n' +
	'        <div class="row no-padder">\n' +
	'          <div class="col-sm-6">\n' +
	'            <div class="call-center w-md m-l-r-auto small">\n' +
	'              <div>Единый контакт-центр</div>\n' +
	'              <a href="http://1414.kz" target="_blank"><ins>1414</ins></a> <strong>8-800-080-7777</strong>\n' +
	'              <div>звонок бесплатный</div>\n' +
	'            </div>\n' +
	'          </div>\n' +
	'          <div class="col-sm-6 wrapper-sm text-center">\n' +
	'                           <div class="search orange w-full m-l-r-auto" id="search">\n' +
	'                <div class="region region-search">\n' +
	'  <div id="block-search-solr-searchsolr" class="block block-search-solr">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    \n' +
	'<div class="search orange w-full m-l-r-auto" id="search">\n' +
	'  <div class="block-search">\n' +
	'      <form action="/searchsolr/search" method="get">\n' +
	'        <div class="container-inline">\n' +
	'          <div class="form-item form-group">\n' +
	'            <input type="text" name="text" value="" size="15" maxlength="128" class="form-text form-control w-full" placeholder="Поиск на сайте">\n' +
	'            <input type="hidden" name="domain" value="http://kgd.gov.kz" placeholder="Поиск на сайте">\n' +
	'          </div>\n' +
	'        </div>\n' +
	'      </form>\n' +
	'  </div> <!-- /.block -->\n' +
	'</div>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'                <span class="clear glyphicon glyphicon-remove-circle"></span>\n' +
	'              </div>\n' +
	'                      </div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'    <div id="smartmenu" class="row">\n' +
	'      <div class="wrapper-nav"><div id="navIco" class="col-md-11 hidden-md hidden-lg"><i class="fa fa-navicon text-left text-boss"></i></div></div>\n' +
	'      <div id="navi" class="col-md-11">\n' +
	'        <nav id="main-nav" role="navigation">\n' +
	'                      <div class="region region-top-menu">\n' +
	'  <div id="block-smartmenus-smartmenus-1" class="block block-smartmenus">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <ul id="smartmenus_1" class="sm sm-kgd" data-smartmenus-id="15891105248652234"><li class="first"><a title="" class="nolink has-submenu" id="sm-15891105248652234-1" aria-haspopup="true" aria-controls="sm-15891105248652234-2" aria-expanded="false">О нас</a><ul class="menu" id="sm-15891105248652234-2" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-1" aria-expanded="false"><li class="first"><a href="/ru/content/polozhenie-o-komitete" title="">Положение о комитете</a></li>\n' +
	'<li class=""><a href="/ru/content/rukovodstvo" title="">Руководство</a></li>\n' +
	'<li class=""><a href="/ru/content/territorialnye-organy" title="">Структура</a></li>\n' +
	'<li class=""><a href="/ru/content/strategiya" title="">Стратегия</a></li>\n' +
	'<li class=""><a href="/ru/section/kadrovoe-obespechenie" title="" class="has-submenu" id="sm-15891105248652234-3" aria-haspopup="true" aria-controls="sm-15891105248652234-4" aria-expanded="false"><span class="sub-arrow">+</span>Кадровое обеспечение</a><ul class="menu" id="sm-15891105248652234-4" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-3" aria-expanded="false"><li class="first"><a href="/ru/content/normativnye-pravovye-akty" title="">Нормативные правовые акты</a></li>\n' +
	'<li class=""><a href="/ru/content/primernye-testovye-voprosy" title="">Примерные тестовые вопросы</a></li>\n' +
	'<li class=""><a href="/ru/content/informaciya-po-voprosam-kadrovogo-obespecheniya-1" title="">Информация по вопросам кадрового обеспечения</a></li>\n' +
	'<li class=""><a href="/ru/content/kvalifikacionnye-trebovaniya-1" title="">Квалификационные требования</a></li>\n' +
	'<li class=""><a href="/ru/section/konkursy-na-vakantnye-dolzhnosti" title="">Конкурсы на вакантные должности</a></li>\n' +
	'<li class="last"><a href="/ru/content/kontakty-1" title="">Контакты</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/podvedomstvennye-organizacii" title="" class="has-submenu" id="sm-15891105248652234-5" aria-haspopup="true" aria-controls="sm-15891105248652234-6" aria-expanded="false"><span class="sub-arrow">+</span>Подведомственные организации</a><ul class="menu" id="sm-15891105248652234-6" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-5" aria-expanded="false"><li class="first"><a href="/ru/section/kinologicheskiy-centr" title="">Кинологический центр</a></li>\n' +
	'<li class=""><a href="/ru/section/uchebno-metodicheskiy-centr" title="" class="has-submenu" id="sm-15891105248652234-7" aria-haspopup="true" aria-controls="sm-15891105248652234-8" aria-expanded="false"><span class="sub-arrow">+</span>Учебно-методический центр</a><ul class="menu" id="sm-15891105248652234-8" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-7" aria-expanded="false"><li class="first"><a href="/ru/content/vakansii-1" title="">Вакансии</a></li>\n' +
	'<li class=""><a href="/ru/content/grafik-priema-upolnomochennogo-po-etike-1-1" title="">График приема уполномоченного по этике  государственных служащих и иных граждан</a></li>\n' +
	'<li class=""><a href="/ru/content/mezhdunarodnoe-sotrudnichestvo-1" title="">Международное сотрудничество</a></li>\n' +
	'<li class=""><a href="/ru/content/polozhenie-1-1" title="">Положение об Учебно-методическом центре</a></li>\n' +
	'<li class=""><a href="/ru/content/professionalnoe-razvitie-1" title="">Профессиональное развитие</a></li>\n' +
	'<li class="last"><a href="/ru/content/uchebnye-plany-umc-1" title="">Учебные планы УМЦ</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/centralnaya-tamozhennaya-laboratoriya" title="">Центральная таможенная лаборатория</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/informaciya-o-tekushchey-deyatelnosti" title="" class="has-submenu" id="sm-15891105248652234-9" aria-haspopup="true" aria-controls="sm-15891105248652234-10" aria-expanded="false"><span class="sub-arrow">+</span>Имидж госслужащего</a><ul class="menu" id="sm-15891105248652234-10" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-9" aria-expanded="false"><li class="first"><a href="/ru/section/poslaniya" title="">Послания</a></li>\n' +
	'<li class=""><a href="/ru/content/obshchenacionalnyy-plan-1" title="">Общенациональный план</a></li>\n' +
	'<li class=""><a href="/ru/section/strategicheskiy-plan" title="">Стратегический план</a></li>\n' +
	'<li class=""><a href="/ru/section/otchety-ob-ispolnenii-strategicheskogo-plana" title="">Отчеты об исполнении Стратегического плана</a></li>\n' +
	'<li class=""><a href="/ru/content/operacionnyy-plan-1" title="">Операционный план</a></li>\n' +
	'<li class=""><a href="/ru/content/zelenaya-ekonomika-1" title="">«Зеленая экономика»</a></li>\n' +
	'<li class=""><a href="/ru/content/ocenka-effektivnosti-deyatelnosti-gosudarstvennogo-organa-po-realizacii-gosudarstvennoy" title="">Оценка эффективности деятельности</a></li>\n' +
	'<li class="last"><a href="/ru/content/plan-raboty-kgd-mf-rk-1" title="">Плана работы Комитета государственных доходов Министерства финансов Республики Казахстан на 2018 год</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/gosudarstvennye-zakupki" title="" class="has-submenu" id="sm-15891105248652234-11" aria-haspopup="true" aria-controls="sm-15891105248652234-12" aria-expanded="false"><span class="sub-arrow">+</span>Государственные закупки</a><ul class="menu" id="sm-15891105248652234-12" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-11" aria-expanded="false"><li class="first"><a href="/ru/content/godovoy-plan-gosudarstvennyh-zakupok" title="">Годовой план государственных закупок</a></li>\n' +
	'<li class=""><a href="/ru/section/normativnye-pravovye-akty" title="">Нормативные правовые акты</a></li>\n' +
	'<li class=""><a href="/ru/section/protokola" title="" class="has-submenu" id="sm-15891105248652234-13" aria-haspopup="true" aria-controls="sm-15891105248652234-14" aria-expanded="false"><span class="sub-arrow">+</span>Протокола</a><ul class="menu" id="sm-15891105248652234-14" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-13" aria-expanded="false"><li class="first"><a href="/ru/content/zapros-cenovyh-predlozheniy-1-0" title="">Запрос ценовых предложений</a></li>\n' +
	'<li class=""><a href="/ru/content/iz-odnogo-istochnika-1" title="">Из одного источника</a></li>\n' +
	'<li class="last"><a href="/ru/content/otkrytyy-konkurs-1-0" title="">Открытый конкурс</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/konkursy-gosudarstvennyh-zakupok" title="" class="has-submenu" id="sm-15891105248652234-15" aria-haspopup="true" aria-controls="sm-15891105248652234-16" aria-expanded="false"><span class="sub-arrow">+</span>Конкурсы государственных закупок</a><ul class="menu" id="sm-15891105248652234-16" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-15" aria-expanded="false"><li class="first"><a href="/ru/content/zapros-cenovyh-predlozheniy-1" title="">Запрос ценовых предложений</a></li>\n' +
	'<li class="last"><a href="/ru/content/otkrytyy-konkurs-1" title="">Открытый конкурс</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a title="" class="nolink has-submenu" id="sm-15891105248652234-17" aria-haspopup="true" aria-controls="sm-15891105248652234-18" aria-expanded="false"><span class="sub-arrow">+</span>Работа с населением</a><ul class="menu" id="sm-15891105248652234-18" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-17" aria-expanded="false"><li class="first"><a href="/ru/content/priem-i-poryadok-rassmotreniya-grazhdan" title="">График приема физических лиц и представителей юридических лиц</a></li>\n' +
	'<li class=""><a href="/ru/content/spravka-o-rabote-s-obrashcheniyami-grazhdan-1" title="">Справка о работе с обращениями граждан</a></li>\n' +
	'<li class=""><a href="/ru/section/dosudebnoe-obzhalovanie-rezultatov-nalogovoy-i-tamozhennoy-proverki" title="">Досудебное обжалование результатов налоговой и таможенной проверки</a></li>\n' +
	'<li class=""><a href="/ru/content/elektronnye-obrashcheniya-1" title="">"Электронные обращения"</a></li>\n' +
	'<li class=""><a href="/ru/content/telefony-doveriya-1" title="">Телефоны доверия</a></li>\n' +
	'<li class=""><a href="/ru/content/spisok-telefonov-dlya-konsultacii-1" title="">Контактные данные  по разъяснению  об общем декларировании</a></li>\n' +
	'<li class="last"><a href="/ru/content/telefony-kontakt-centrov-organov-gosudarstvennyh-dohodov-1" title="">Телефоны контакт-центров органов государственных доходов</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/press-centr" title="" class="has-submenu" id="sm-15891105248652234-19" aria-haspopup="true" aria-controls="sm-15891105248652234-20" aria-expanded="false"><span class="sub-arrow">+</span>Пресс-центр</a><ul class="menu" id="sm-15891105248652234-20" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-19" aria-expanded="false"><li class="first"><a href="/ru/section/akciya-trebuy-chek-vyigray-priz" title="">Акция "Требуй чек – выиграй приз!"</a></li>\n' +
	'<li class=""><a href="/ru/content/vestnik-organov-gosudarstvennyh-dohodov-respubliki-kazahstan-1" title="">Вестник</a></li>\n' +
	'<li class=""><a href="/ru/content/doklady-1" title="">Доклады</a></li>\n' +
	'<li class=""><a href="/ru/content/nashi-veterany-1" title="">Наши ветераны</a></li>\n' +
	'<li class=""><a href="/ru/section/pisma-razyasnitelnogo-haraktera" title="">Письма разъяснительного характера</a></li>\n' +
	'<li class=""><a href="/ru/content/proekt-ramochnogo-memoranduma-o-vzaimoponimanii-i-sotrudnichestve-1" title="">Проект Рамочного меморандума о взаимопонимании и сотрудничестве</a></li>\n' +
	'<li class=""><a href="/ru/video" title="">Видеоматериалы</a></li>\n' +
	'<li class=""><a href="/ru/photos" title="">Фотогалерея</a></li>\n' +
	'<li class=""><a href="/ru/content/intervyu-stati-1" title="">Интервью, статьи</a></li>\n' +
	'<li class="last"><a href="/ru/content/seminary-1" title="">Семинары</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/kontakty-1-0" title="">Контакты</a></li>\n' +
	'<li class=""><a href="/ru/section/reyting-doing-business" title="">Рейтинг ГИК ВЭФ</a></li>\n' +
	'<li class="last"><a href="/ru/content/upolnomochennyy-po-etike-1" title="">Уполномоченный по этике</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/deyatelnost" title="" class="has-submenu" id="sm-15891105248652234-21" aria-haspopup="true" aria-controls="sm-15891105248652234-22" aria-expanded="false">Деятельность</a><ul class="menu" id="sm-15891105248652234-22" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-21" aria-expanded="false"><li class="first"><a href="/ru/section/informacionnye-sistemy" title="" class="has-submenu" id="sm-15891105248652234-23" aria-haspopup="true" aria-controls="sm-15891105248652234-24" aria-expanded="false"><span class="sub-arrow">+</span>Информационные системы</a><ul class="menu" id="sm-15891105248652234-24" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-23" aria-expanded="false"><li class="first"><a href="/ru/section/sono" title="" class="has-submenu" id="sm-15891105248652234-25" aria-haspopup="true" aria-controls="sm-15891105248652234-26" aria-expanded="false"><span class="sub-arrow">+</span>ИС СОНО</a><ul class="menu" id="sm-15891105248652234-26" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-25" aria-expanded="false"><li class="first"><a href="/ru/content/pravila-oformleniya-obrashcheniya-na-sluzhbu-podderzhki-sono-1" title="">Правила оформления обращения на Службу поддержки ИС СОНО</a></li>\n' +
	'<li class="last"><a href="/ru/section/programmnoe-obespechenie" title="">Программное обеспечение</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/ukm" title="">ИС УКМ</a></li>\n' +
	'<li class=""><a href="/ru/section/akcizy-0" title="">ИС Акцизы</a></li>\n' +
	'<li class=""><a href="/ru/content/programmnoe-obespechenie-1-0" title="">ИС ЭСФ</a></li>\n' +
	'<li class=""><a href="/ru/content/astana-1-1" title="">ИС АСТАНА-1</a></li>\n' +
	'<li class="last"><a href="/ru/content/nds-blockchain-1" title="">ИС НДС-Blockchain</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/spravochniki-0" title="">Справочники</a></li>\n' +
	'<li class=""><a href="/ru/section/statistika" title="" class="has-submenu" id="sm-15891105248652234-27" aria-haspopup="true" aria-controls="sm-15891105248652234-28" aria-expanded="false"><span class="sub-arrow">+</span>Статистика</a><ul class="menu" id="sm-15891105248652234-28" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-27" aria-expanded="false"><li class="first"><a href="/ru/content/dinamika-postupleniy-nalogov-i-platezhey-v-gosudarstvennyy-byudzhet-1" title="">Динамика поступлений налогов и платежей в государственный бюджет</a></li>\n' +
	'<li class=""><a href="/ru/content/fakticheskie-postupleniya-po-nalogam-i-platezham-v-gosudarstvennyy-byudzhet-za-2002-2018-gg" title="">Фактические поступления по налогам и платежам в государственный бюджет</a></li>\n' +
	'<li class=""><a href="/ru/section/dinamika-postupleniy-nalogov-i-platezhey-v-nacionalnyy-fond" title="">Динамика поступлений налогов и платежей в Национальный фонд</a></li>\n' +
	'<li class=""><a href="/ru/content/korporativnyy-podohodnyy-nalog-1-7" title="">Корпоративный подоходный налог</a></li>\n' +
	'<li class="last"><a title="" class="nolink has-submenu" id="sm-15891105248652234-29" aria-haspopup="true" aria-controls="sm-15891105248652234-30" aria-expanded="false"><span class="sub-arrow">+</span>Статистика внешней торговли РК</a><ul class="menu" id="sm-15891105248652234-30" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-29" aria-expanded="false"><li class="first"><a href="/ru/exp_trade_files" title="">Показатели внешней торговли</a></li>\n' +
	'<li class="last"><a href="/ru/exp_trade/report" title="">Отчеты</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/spiski-platelshchikov-bankrotov" title="" class="has-submenu" id="sm-15891105248652234-31" aria-haspopup="true" aria-controls="sm-15891105248652234-32" aria-expanded="false"><span class="sub-arrow">+</span>Списки плательщиков</a><ul class="menu" id="sm-15891105248652234-32" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-31" aria-expanded="false"><li class="first"><a href="/ru/section/spisok-opredelennyh-kategoriy-dolzhnikov-ne-pogashayushchih-zadolzhennost-dlitelnoe-vremya" title="" class="has-submenu" id="sm-15891105248652234-33" aria-haspopup="true" aria-controls="sm-15891105248652234-34" aria-expanded="false"><span class="sub-arrow">+</span>Информация о налогоплательщиках, имеющих налоговую задолженность, задолженность по ОПВ, ОППВ не погашенную в течение 6 месяцев.</a><ul class="menu" id="sm-15891105248652234-34" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-33" aria-expanded="false"><li class="first"><a href="/ru/content/2018-god-1" title="">2018 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2016-god-1" title="">2016 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2017-god-1-2" title="">2017 год</a></li>\n' +
	'<li class=""><a href="/ru/content/spisok-nalogoplatelshchikov-imeyushchih-nalogovuyu-zadolzhennost" title="">2015 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2014-god-1-0" title="">2014 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2020-god-1-2" title="">2019 год</a></li>\n' +
	'<li class="last"><a href="/ru/content/2020-god-1-4" title="">2020 год</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/perechen-lzhepredpriyatiy" title="">Перечень лжепреприятий</a></li>\n' +
	'<li class=""><a href="/ru/content/spiski-uvedomleniy-1" title="">Списки уведомлений</a></li>\n' +
	'<li class=""><a href="/ru/section/cpisok-platelshchikov-imeyushchih-zadolzhennost-po-tamozhennym-platezham-nalogam-i-penyam" title="" class="has-submenu" id="sm-15891105248652234-35" aria-haspopup="true" aria-controls="sm-15891105248652234-36" aria-expanded="false"><span class="sub-arrow">+</span>Cписок плательщиков, имеющих задолженность по таможенным платежам, налогам и пеням</a><ul class="menu" id="sm-15891105248652234-36" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-35" aria-expanded="false"><li class="first"><a href="/ru/content/2017-god-1-1" title="">2017 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2016-god-1-1" title="">2016 год</a></li>\n' +
	'<li class=""><a href="/ru/content/2018-god-1-0" title="">2018 год</a></li>\n' +
	'<li class="last"><a href="/ru/content/2020-god-1-3" title="">2019 год</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/spiski-nesostoyatelnyh-dolzhnikov" title="">Списки несостоятельных должников</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/o-realizacii-pyati-reform-100-shagov" title="">О реализации Пяти реформ "100 шагов"</a></li>\n' +
	'<li class=""><a href="/ru/section/tamozhennoe-administrirovanie" title="" class="has-submenu" id="sm-15891105248652234-37" aria-haspopup="true" aria-controls="sm-15891105248652234-38" aria-expanded="false"><span class="sub-arrow">+</span>Таможенное администрирование</a><ul class="menu" id="sm-15891105248652234-38" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-37" aria-expanded="false"><li class="first"><a href="/ru/content/avtodilery-v-rk-1" title="">Автодилеры в РК</a></li>\n' +
	'<li class=""><a href="/ru/content/sistema-upravleniya-riskami-1-0" title="">Система управления рисками</a></li>\n' +
	'<li class=""><a href="/ru/section/tamozhennoe-oformlenie" title="" class="has-submenu" id="sm-15891105248652234-39" aria-haspopup="true" aria-controls="sm-15891105248652234-40" aria-expanded="false"><span class="sub-arrow">+</span>Таможенное оформление</a><ul class="menu" id="sm-15891105248652234-40" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-39" aria-expanded="false"><li class="first"><a href="/ru/section/fizicheskim-licam" title="">Физические лица</a></li>\n' +
	'<li class=""><a href="/ru/section/yuridicheskie-lica" title="">Юридические лица</a></li>\n' +
	'<li class="last"><a href="/ru/section/klassifikaciya-i-proishozhdenie-tovarov" title="" class="has-submenu" id="sm-15891105248652234-41" aria-haspopup="true" aria-controls="sm-15891105248652234-42" aria-expanded="false"><span class="sub-arrow">+</span>Евразийский экономический союз</a><ul class="menu" id="sm-15891105248652234-42" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-41" aria-expanded="false"><li class="first"><a href="/ru/content/voprosy-otvety-v-chasti-klassifikacii-i-proishozhdeniya-tovarov-1" title="">Вопросы  и ответы</a></li>\n' +
	'<li class=""><a href="/ru/content/normativnye-pravovye-akty-po-voprosam-klassifikacii-1-0" title="">Нормативные правовые акты по вопросам классификации</a></li>\n' +
	'<li class=""><a href="/ru/content/resheniya-kollegii-evraziyskoy-ekonomicheskoy-komissii-po-klassifikacii-tovarov-1-0" title="">Решения Коллегии Евразийской экономической комиссии</a></li>\n' +
	'<li class=""><a href="/ru/content/resheniya-obedinennoy-kollegii-tamozhennyh-sluzhb-gosudarstv-chlenov-tamozhennogo-soyuza-po" title="">Решения Объединенной Коллегии таможенных служб государств-членов Таможенного союза по классификации товаров</a></li>\n' +
	'<li class=""><a href="/ru/content/sbornik-soglasheniy-po-opredeleniyu-strany-proishozhdeniya-tovarov-1" title="">Сборник соглашений по определению страны происхождения товаров</a></li>\n' +
	'<li class="last"><a href="/ru/content/tn-ved-i-ett-eaes" title="">Товарная номеклатура  внешнеэкономической деятельности и Единый таможенный тариф Евразийского экономического союза</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/tamozhennyy-kontrol" title="" class="has-submenu" id="sm-15891105248652234-43" aria-haspopup="true" aria-controls="sm-15891105248652234-44" aria-expanded="false"><span class="sub-arrow">+</span>Таможенный контроль</a><ul class="menu" id="sm-15891105248652234-44" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-43" aria-expanded="false"><li class="first"><a href="/ru/content/cenovaya-informaciya-1" title="">Ценовая информация</a></li>\n' +
	'<li class=""><a href="/ru/content/voprosy-po-kvalifikacionnym-ekzamenam-specialistov-po-tamozhennomu-deklarirovaniyu-1" title="">Вопросы по квалификационным экзаменам специалистов по таможенному декларированию</a></li>\n' +
	'<li class=""><a href="/ru/content/perechen-mest-tamozhennogo-oformleniya-1" title="">Перечень мест таможенного оформления</a></li>\n' +
	'<li class=""><a href="/ru/content/perechen-tamozhennyh-predstaviteley-narushivshih-tamozhennoe-zakonodatelstvo-rk-1" title="">Перечень таможенных представителей, нарушивших таможенное законодательство РК</a></li>\n' +
	'<li class=""><a href="/ru/section/reestry" title="">Реестры</a></li>\n' +
	'<li class="last"><a href="/ru/content/srednyaya-rynochnaya-cena-syroy-nefti-1" title="">Средняя рыночная цена сырой нефти</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/voprosy-protivodeystviya-korrupcii" title="">Вопросы противодействия коррупции</a></li>\n' +
	'<li class=""><a href="/ru/section/vsemirnyy-bank" title="" class="has-submenu" id="sm-15891105248652234-45" aria-haspopup="true" aria-controls="sm-15891105248652234-46" aria-expanded="false"><span class="sub-arrow">+</span>Всемирный банк</a><ul class="menu" id="sm-15891105248652234-46" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-45" aria-expanded="false"><li class="first"><a href="/ru/section/zakupki-prna" title="">Закупки ПРНА</a></li>\n' +
	'<li class="last"><a href="/ru/section/zakupki-prts" title="">Закупки ПРТС</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/legalizaciya" title="" class="has-submenu" id="sm-15891105248652234-47" aria-haspopup="true" aria-controls="sm-15891105248652234-48" aria-expanded="false"><span class="sub-arrow">+</span>Легализация</a><ul class="menu" id="sm-15891105248652234-48" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-47" aria-expanded="false"><li class="first"><a href="/ru/content/itogi-legalizacii-1" title="">Итоги легализации</a></li>\n' +
	'<li class=""><a href="/ru/content/telefony-komissiy-po-legalizacii-1" title="">Телефоны комиссий по легализации</a></li>\n' +
	'<li class=""><a href="/ru/section/vse-o-legalizacii" title="" class="has-submenu" id="sm-15891105248652234-49" aria-haspopup="true" aria-controls="sm-15891105248652234-50" aria-expanded="false"><span class="sub-arrow">+</span>Все о легализации</a><ul class="menu" id="sm-15891105248652234-50" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-49" aria-expanded="false"><li class="first"><a href="/ru/content/voprosy-otvety-1" title="">Вопросы - ответы</a></li>\n' +
	'<li class=""><a href="/ru/content/itogi-proshlyh-legalizaciy-v-strane-1" title="">Итоги прошлых легализаций в стране</a></li>\n' +
	'<li class=""><a href="/ru/content/legalizaciya-2014-otlichiya-1" title="">Легализация 2014 – отличия</a></li>\n' +
	'<li class=""><a href="/ru/content/legalizaciya-vygodna-kak-dlya-grazhdan-tak-i-dlya-strany-1" title="">Легализация выгодна, как для граждан, так и для страны</a></li>\n' +
	'<li class=""><a href="/ru/content/legalizaciya-bez-boyazni-byt-privlechennym-k-otvetstvennosti-1" title="">Легализация – без боязни быть привлеченным к ответственности</a></li>\n' +
	'<li class=""><a href="/ru/content/legalizaciya-ne-novshestvo-v-mirovoy-ekonomike-1" title="">Легализация – не новшество в мировой экономике</a></li>\n' +
	'<li class=""><a href="/ru/content/legalizaciya-v-tom-chisle-eto-i-uproshchenie-procedury-gosregistracii-1" title="">Легализация, в том числе, – это и упрощение процедуры госрегистрации</a></li>\n' +
	'<li class=""><a href="/ru/content/nemnogo-o-legalizacii-imushchestva-1" title="">Немного о легализации имущества</a></li>\n' +
	'<li class="last"><a href="/ru/content/o-poryadke-legalizacii-imushchestva-v-kazahstane-1" title="">О порядке легализации имущества в Казахстане</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/zakonodatelstvo-po-legalizacii" title="">Законодательство по легализации</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/nalogovoe-administrirovanie" title="" class="has-submenu" id="sm-15891105248652234-51" aria-haspopup="true" aria-controls="sm-15891105248652234-52" aria-expanded="false"><span class="sub-arrow">+</span>Налоговое администрирование</a><ul class="menu" id="sm-15891105248652234-52" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-51" aria-expanded="false"><li class="first"><a href="/ru/content/primenenie-kontrolno-kassovyh-mashin-1" title="">Применение контрольно-кассовых машин</a></li>\n' +
	'<li class=""><a href="/ru/content/primenenie-pos-terminalov-1" title="">Применение POS-терминалов</a></li>\n' +
	'<li class=""><a href="/ru/content/perechen-krupnyh-nalogoplatelshchikov-podlezhashchih-monitoringu-1" title="">Перечень крупных налогоплательщиков, подлежащих мониторингу</a></li>\n' +
	'<li class=""><a href="/ru/section/akcizy" title="">Акцизы</a></li>\n' +
	'<li class=""><a href="/ru/content/koefficient-nalogovoy-nagruzki-1-0" title="">Коэффициент налоговой нагрузки</a></li>\n' +
	'<li class=""><a href="/ru/section/nalogovye-proverki" title="" class="has-submenu" id="sm-15891105248652234-53" aria-haspopup="true" aria-controls="sm-15891105248652234-54" aria-expanded="false"><span class="sub-arrow">+</span>Налоговые проверки</a><ul class="menu" id="sm-15891105248652234-54" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-53" aria-expanded="false"><li class="first"><a href="/ru/content/obyazatelnaya-vedomstvennaya-otchetnost-1" title="">Обязательная ведомственная отчетность</a></li>\n' +
	'<li class="last"><a href="/ru/content/luchshie-rabotniki-nalogovogo-audita-territorialnyh-departamentov-gosudarstvennyh-dohodov-1" title="">Лучшие работники налогового аудита территориальных департаментов государственных доходов</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/nalogooblozhenie-nerezidentov" title="">Налогообложение нерезидентов</a></li>\n' +
	'<li class=""><a href="/ru/section/nedropolzovanie" title="">Недропользование</a></li>\n' +
	'<li class=""><a href="/ru/section/otsrochka-uplaty-pri-importe-iz-tretih-stran" title="">Отсрочка уплаты при импорте из третьих стран</a></li>\n' +
	'<li class=""><a href="/ru/section/tamozhennyy-soyuz" title="" class="has-submenu" id="sm-15891105248652234-55" aria-haspopup="true" aria-controls="sm-15891105248652234-56" aria-expanded="false"><span class="sub-arrow">+</span>Таможенный союз</a><ul class="menu" id="sm-15891105248652234-56" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-55" aria-expanded="false"><li class="first"><a href="/ru/content/mezhdunarodnye-dogovory-v-ramkah-tamozhennogo-soyuza" title="">Международные договоры в рамках Таможенного союза</a></li>\n' +
	'<li class="last"><a href="/ru/content/postanovleniya-pravitelstva-respubliki-kazahstan-0" title="">Постановления Правительства Республики Казахстан</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/content/transfertnoe-cenoobrazovanie-1-1" title="">Трансфертное ценообразование</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/vto" title="">ВТО</a></li>\n' +
	'<li class=""><a href="/ru/section/mezhdunarodnoe-sotrudnichestvo" title="" class="has-submenu" id="sm-15891105248652234-57" aria-haspopup="true" aria-controls="sm-15891105248652234-58" aria-expanded="false"><span class="sub-arrow">+</span>Международное сотрудничество</a><ul class="menu" id="sm-15891105248652234-58" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-57" aria-expanded="false"><li class="first"><a href="/ru/imap" title="">Интерактивная карта</a></li>\n' +
	'<li class="last"><a href="/ru/content/organizaciya-ekonomicheskogo-sotrudnichestva-i-razvitiya-1" title="">ОРГАНИЗАЦИЯ ЭКОНОМИЧЕСКОГО СОТРУДНИЧЕСТВА И РАЗВИТИЯ</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/poryadok-raboty" title="">Порядок работы</a></li>\n' +
	'<li class=""><a href="http://kgd.gov.kz/ru/section/grazhdanam" title="">Гражданам</a></li>\n' +
	'<li class=""><a href="/ru/section/monitoring-inostrannogo-finansirovaniya" title="">Мониторинг иностранного финансирования</a></li>\n' +
	'<li class=""><a href="http://kgd.gov.kz/ru/section/pravoohranitelnaya-deyatelnost" title="">Правоохранительная деятельность</a></li>\n' +
	'<li class=""><a href="/ru/section/vseobshchee-deklarirovanie" title="">Всеобщее декларирование</a></li>\n' +
	'<li class=""><a href="/ru/content/edinyy-sovokupnyy-platezh-1" title="">Единый совокупный платеж</a></li>\n' +
	'<li class=""><a href="/ru/section/markirovka" title="" class="has-submenu" id="sm-15891105248652234-59" aria-haspopup="true" aria-controls="sm-15891105248652234-60" aria-expanded="false"><span class="sub-arrow">+</span>Маркировка</a><ul class="menu" id="sm-15891105248652234-60" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-59" aria-expanded="false"><li class="first"><a href="/ru/content/markirovka-i-proslezhivaemost-tabachnyh-izdeliy-1" title="">Маркировка и прослеживаемость табачных изделий</a></li>\n' +
	'<li class=""><a href="/ru/content/markirovka-mehovyh-izdeliy-1" title="">Маркировка меховых изделий</a></li>\n' +
	'<li class="last"><a href="/ru/content/materialy-1" title="">Материалы</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/mezhdunarodnyy-avtomaticheskiy-obmen-1" title="">Международный автоматический обмен</a></li>\n' +
	'<li class=""><a href="/ru/content/nalogovaya-amnistiya-dlya-subektov-malogo-i-srednego-biznesa-1" title="">Налоговая амнистия</a></li>\n' +
	'<li class=""><a href="/ru/content/trehkomponentnaya-integrirovannaya-sistema-1-0" title="">Трехкомпонентная интегрированная система</a></li>\n' +
	'<li class="last"><a href="/ru/section/elektronnaya-torgovlya" title="" class="has-submenu" id="sm-15891105248652234-61" aria-haspopup="true" aria-controls="sm-15891105248652234-62" aria-expanded="false"><span class="sub-arrow">+</span>Электронная торговля</a><ul class="menu" id="sm-15891105248652234-62" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-61" aria-expanded="false"><li class="first"><a href="/ru/content/zakonodatelstvo-1-1" title="">Законодательство</a></li>\n' +
	'<li class=""><a href="/ru/content/soputstvuyushchie-normativnye-pravovye-akty-1" title="">Сопутствующие нормативные правовые акты</a></li>\n' +
	'<li class=""><a href="/ru/content/prezentaciya-nalogovogo-administrirovaniya-elektronnoy-torgovli-1" title="">Презентация налогового администрирования электронной торговли</a></li>\n' +
	'<li class="last"><a href="/ru/content/forma-uvedomleniya-o-nachale-deyatelnosti-v-kachestve-nalogoplatelshchika" title="">Форма уведомления о начале деятельности в качестве налогоплательщика, осуществляющего электронную торговлю товарами</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/zakonodatelstvo-rk" title="" class="has-submenu" id="sm-15891105248652234-63" aria-haspopup="true" aria-controls="sm-15891105248652234-64" aria-expanded="false">Законодательство РК</a><ul class="menu" id="sm-15891105248652234-64" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-63" aria-expanded="false"><li class="first"><a href="/ru/section/zakony-rk" title="">Законы РК</a></li>\n' +
	'<li class=""><a href="/ru/content/nalogovyy-kodeks-rk" title="">Налоговый кодекс РК</a></li>\n' +
	'<li class=""><a href="/ru/content/kodeks-rk-o-tamozhennom-dele-v-rk" title="">Кодекс РК «О таможенном деле в РК»</a></li>\n' +
	'<li class=""><a href="/ru/content/ugolovno-processualnyy-kodeks-rk" title="">Уголовно-процессуальный кодекс РК</a></li>\n' +
	'<li class=""><a href="/ru/content/ugolovnyy-kodeks-rk-0" title="">Уголовный Кодекс РК</a></li>\n' +
	'<li class=""><a href="/ru/content/kodeks-respubliki-kazahstan-ob-administrativnyh-pravonarusheniyah-1" title="">Кодекс РК «Об административных правонарушениях»</a></li>\n' +
	'<li class=""><a href="/ru/section/kommentariy-k-nalogovomu-kodeksu-rk" title="">Комментарий к Налоговому кодексу РК</a></li>\n' +
	'<li class=""><a href="/ru/section/formy-nalogovoy-otchetnosti" title="" class="has-submenu" id="sm-15891105248652234-65" aria-haspopup="true" aria-controls="sm-15891105248652234-66" aria-expanded="false"><span class="sub-arrow">+</span>Формы налоговой отчетности</a><ul class="menu" id="sm-15891105248652234-66" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-65" aria-expanded="false"><li class="first"><a href="/ru/section/fno-2019-god" title="">ФНО 2019 год</a></li>\n' +
	'<li class=""><a href="/ru/section/fno-2018-god" title="">ФНО 2018 год</a></li>\n' +
	'<li class=""><a href="/ru/section/fno-2017-god" title="">ФНО 2017 год</a></li>\n' +
	'<li class=""><a href="/ru/section/fno-2015-2016-gody" title="">ФНО 2015-2016 годы</a></li>\n' +
	'<li class=""><a href="/ru/section/fno-2014-god" title="">ФНО 2014 год</a></li>\n' +
	'<li class=""><a href="/ru/section/fno-2013-god" title="">ФНО 2013 год</a></li>\n' +
	'<li class="last"><a href="/ru/section/fno-2020-god" title="">ФНО 2020 год</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/section/normativnye-pravovye-akty-1" title="" class="has-submenu" id="sm-15891105248652234-67" aria-haspopup="true" aria-controls="sm-15891105248652234-68" aria-expanded="false"><span class="sub-arrow">+</span>Нормативные правовые акты</a><ul class="menu" id="sm-15891105248652234-68" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-67" aria-expanded="false"><li class="first"><a href="/ru/content/postanovleniya-pravitelstva-rk" title="">Постановления Правительства РК</a></li>\n' +
	'<li class=""><a href="/ru/section/prikazy" title="" class="has-submenu" id="sm-15891105248652234-69" aria-haspopup="true" aria-controls="sm-15891105248652234-70" aria-expanded="false"><span class="sub-arrow">+</span>Приказы</a><ul class="menu" id="sm-15891105248652234-70" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-69" aria-expanded="false"><li class="first"><a href="/ru/content/prikazy-2019-goda-1" title="">Приказы  2019 года</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy-2020-god-1" title="">Приказы  2020 год</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy-2020-god-1-0" title="">Приказы  2020 год</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy" title="">Приказы 2014 года</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy-2015-goda-1" title="">Приказы 2015 года</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy-2016-goda-1" title="">Приказы 2016 года</a></li>\n' +
	'<li class=""><a href="/ru/content/prikazy-2017-goda-1" title="">Приказы 2017 года</a></li>\n' +
	'<li class="last"><a href="/ru/content/prikazy-2018-goda-1" title="">Приказы 2018 года</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="http://kgd.gov.kz/ru/content/proekty-normativnyh-pravovyh-aktov" title="">Проекты нормативных правовых актов</a></li>\n' +
	'<li class=""><a href="/ru/content/nalogovye-zayavleniya" title="">Налоговые заявления</a></li>\n' +
	'<li class="last"><a href="/ru/content/tamozhennye-zayavleniya-1" title="">Таможенные заявления</a></li>\n' +
	'</ul></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/section/reabilitaciya-i-bankrotstvo" title="" class="has-submenu" id="sm-15891105248652234-71" aria-haspopup="true" aria-controls="sm-15891105248652234-72" aria-expanded="false">Реабилитация и банкротство</a><ul class="menu" id="sm-15891105248652234-72" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-71" aria-expanded="false"><li class="first"><a href="/ru/section/reestry-trebovaniy-kreditorov-utverzhdennyh-v-ramkah-zakona-respubliki-kazahstan-o" title="">Реестры требований кредиторов, утвержденных в рамках Закона Республики Казахстан "О банкротстве"</a></li>\n' +
	'<li class=""><a href="/ru/section/territorialnye-organy" title="" class="has-submenu" id="sm-15891105248652234-73" aria-haspopup="true" aria-controls="sm-15891105248652234-74" aria-expanded="false"><span class="sub-arrow">+</span>Территориальные органы</a><ul class="menu" id="sm-15891105248652234-74" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-73" aria-expanded="false"><li class="first"><a href="/ru/section/akmolinskaya-oblast" title="">Акмолинская область</a></li>\n' +
	'<li class=""><a href="/ru/section/aktyubinskaya-oblast" title="">Актюбинская область</a></li>\n' +
	'<li class=""><a href="/ru/section/almatinskaya-oblast" title="">Алматинская область</a></li>\n' +
	'<li class=""><a href="/ru/section/atyrauskaya-oblast" title="">Атырауская область</a></li>\n' +
	'<li class=""><a href="/ru/section/vostochno-kazahstanskaya-oblast" title="">Восточно-Казахстанская область</a></li>\n' +
	'<li class=""><a href="/ru/section/zhambylskaya-oblast" title="">Жамбылская область</a></li>\n' +
	'<li class=""><a href="/ru/section/zapadno-kazahstanskaya-oblast" title="">Западно-Казахстанская область</a></li>\n' +
	'<li class=""><a href="/ru/section/karagandinskaya-oblast" title="">Карагандинская область</a></li>\n' +
	'<li class=""><a href="/ru/section/kostanayskaya-oblast" title="">Костанайская область</a></li>\n' +
	'<li class=""><a href="/ru/section/kyzylordinskaya-oblast" title="">Кызылординская область</a></li>\n' +
	'<li class=""><a href="/ru/section/mangistauskaya-oblast" title="">Мангистауская область</a></li>\n' +
	'<li class=""><a href="/ru/section/pavlodarskaya-oblast" title="">Павлодарская область</a></li>\n' +
	'<li class=""><a href="/ru/section/severo-kazahstanskaya-oblast" title="">Северо-Казахстанская область</a></li>\n' +
	'<li class=""><a href="/ru/section/yuzhno-kazahstanskaya-oblast" title="">Южно-Казахстанская область</a></li>\n' +
	'<li class=""><a href="/ru/section/galmaty" title="">г.Алматы</a></li>\n' +
	'<li class="last"><a href="/ru/section/gastana" title="">г.Астана</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/voprosy-otvety-1-3" title="">Вопросы – ответы</a></li>\n' +
	'<li class=""><a href="/ru/content/edinyy-reestr-lic-uvedomleniya-kotoryh-vklyucheny-v-reestr-uvedomleniy-lic-imeyushchih-pravo" title="">Единый реестр лиц, уведомления которых включены в реестр уведомлений лиц, имеющих право осуществлять деятельность администратора</a></li>\n' +
	'<li class=""><a href="/ru/content/informacionnye-materialy-1" title="" class="has-submenu" id="sm-15891105248652234-75" aria-haspopup="true" aria-controls="sm-15891105248652234-76" aria-expanded="false"><span class="sub-arrow">+</span>Информационные материалы</a><ul class="menu" id="sm-15891105248652234-76" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-75" aria-expanded="false"><li class="first"><a href="/ru/section/2013-god" title="">2013 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2014-god" title="">2014 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2015-god" title="">2015 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2016-god" title="">2016 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2017-god" title="">2017 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2018-god" title="">2018 год</a></li>\n' +
	'<li class=""><a href="/ru/section/2019-god" title="">2019 год</a></li>\n' +
	'<li class="last"><a href="/ru/content/2020-god-1-6" title="">2020 год</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/normativno-pravovaya-baza-1" title="">Нормативно-правовая база</a></li>\n' +
	'<li class="last"><a href="/ru/content/2019-god-1-3" title="">Реестр саморегулируемых организаций в сфере реабилитации и банкротства</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/territoriya-nalogoplatelshchika-1" title="">ГОСУДАРСТВЕННЫЕ УСЛУГИ - ТЕРРИТОРИЯ НАЛОГОПЛАТЕЛЬЩИКА</a></li>\n' +
	'<li class=""><a href="/ru/section/gik-vef-doing-business" title="" class="has-submenu" id="sm-15891105248652234-77" aria-haspopup="true" aria-controls="sm-15891105248652234-78" aria-expanded="false">Рейтинг «Doing Business»</a><ul class="menu" id="sm-15891105248652234-78" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-77" aria-expanded="false"><li class="first"><a href="/ru/section/reyting-doing-business" title="">Рейтинг «Doing Business»</a></li>\n' +
	'<li class="last"><a href="/ru/section/reyting-gik-vef" title="">Рейтинг ГИК ВЭФ</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="http://kgd.gov.kz/ru/faq" title="">Часто задаваемые вопросы</a></li>\n' +
	'<li class=""><a href="/ru/section/elektronnye-scheta-faktury" title="" class="has-submenu" id="sm-15891105248652234-79" aria-haspopup="true" aria-controls="sm-15891105248652234-80" aria-expanded="false">ИС ЭСФ</a><ul class="menu" id="sm-15891105248652234-80" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-79" aria-expanded="false"><li class="first"><a href="/ru/section/api-interfeys" title="" class="has-submenu" id="sm-15891105248652234-81" aria-haspopup="true" aria-controls="sm-15891105248652234-82" aria-expanded="false"><span class="sub-arrow">+</span>API - интерфейс</a><ul class="menu" id="sm-15891105248652234-82" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-81" aria-expanded="false"><li class="first"><a href="/ru/content/api-interfeys-web-prilozheniya-elektronnye-scheta-faktury-1" title="">API-интерфейс</a></li>\n' +
	'<li class="last"><a href="/ru/content/api-interfeys-web-prilozheniya-elektronnye-scheta-faktury-s-modulem-virtualnyy-sklad-1" title="">Виртуальный склад</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/virtualnyy-sklad-1" title="">Виртуальный склад</a></li>\n' +
	'<li class=""><a href="/ru/content/dokumenty-po-esf-1" title="">Документы по ИС ЭСФ</a></li>\n' +
	'<li class=""><a href="/ru/content/zakonodatelstvo-1" title="">Законодательство</a></li>\n' +
	'<li class=""><a href="/ru/content/informacionnye-materialy-po-sisteme-1" title="">Информационные материалы по системе</a></li>\n' +
	'<li class=""><a href="/ru/content/pilotnyy-proekt-1" title="">Пилотный проект</a></li>\n' +
	'<li class=""><a href="/ru/content/programmnoe-obespechenie-1-0" title="">Программное обеспечение</a></li>\n' +
	'<li class=""><a href="/ru/content/sluzhba-podderzhki-esf-1" title="">Служба поддержки ИС ЭСФ</a></li>\n' +
	'<li class=""><a href="/ru/content/soprovoditelnaya-nakladnaya-na-tovary-1" title="" class="has-submenu" id="sm-15891105248652234-83" aria-haspopup="true" aria-controls="sm-15891105248652234-84" aria-expanded="false"><span class="sub-arrow">+</span>Сопроводительная накладная на товары</a><ul class="menu" id="sm-15891105248652234-84" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-83" aria-expanded="false"><li class="first last"><a href="/ru/content/videouroki-1" title="">Видеоуроки</a></li>\n' +
	'</ul></li>\n' +
	'<li class="last"><a href="/ru/content/testovyy-stend-esf-1" title="">Тестовый стенд ИС ЭСФ</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/predvaritelnye-resheniya-o-klassifikacii-tovarov-1" title="">Предварительные решения о классификации товаров</a></li>\n' +
	'<li class=""><a href="/ru/section/digitalization" title="" class="has-submenu" id="sm-15891105248652234-85" aria-haspopup="true" aria-controls="sm-15891105248652234-86" aria-expanded="false">ЦИФРОВИЗАЦИЯ</a><ul class="menu" id="sm-15891105248652234-86" role="group" aria-hidden="true" aria-labelledby="sm-15891105248652234-85" aria-expanded="false"><li class="first"><a href="/ru/content/poslanie-prezidenta-respubliki-kazahstan-nnazarbaeva-narodu-kazahstana-1" title="">Послание Президента Республики Казахстан</a></li>\n' +
	'<li class=""><a href="/ru/content/organizacionnaya-struktura-upravleniya-1" title="">ОРГАНИЗАЦИОННАЯ СТРУКТУРА УПРАВЛЕНИЯ</a></li>\n' +
	'<li class=""><a href="/ru/content/proekty-1-0" title="">ПРОЕКТЫ</a></li>\n' +
	'<li class="last"><a href="/ru/content/reglament-realizacii-proektov-1" title="">РЕГЛАМЕНТ РЕАЛИЗАЦИИ ПРОЕКТОВ</a></li>\n' +
	'</ul></li>\n' +
	'<li class=""><a href="/ru/content/cenovaya-informaciya-1" title="">Ценовая информация по товарам</a></li>\n' +
	'<li class="last"><a href="/ru/covid-19" title="" class="smartmenu-covid">МЕРЫ ПОДДЕРЖКИ НАЛОГОПЛАТЕЛЬЩИКОВ В ПЕРИОД ЧП (COVID-19)</a></li>\n' +
	'</ul>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'                            </nav>\n' +
	'      </div>\n' +
	'\n' +
	'      <ul class="nav navbar-nav dropr hidden-xs hidden-sm">\n' +
	'        <!-- <li class="dropdown pos-stc open">\n' +
	'          <a href="#" data-toggle="dropdown" class="dropdown-toggle" aria-expanded="true"><i id="toggleMap" class="fa fa-angle-double-down fa-lg text-def cursorHand"></i></a>\n' +
	'        </li> -->\n' +
	'        <li class="dropdown w-full wrapper-xs" data-animation="fadeInUp">\n' +
	'          <a class="dropdown-toggle text-right" data-toggle="dropdown" href="#" data-title="Home">\n' +
	'            <i class="fa fa-angle-double-down fa-lg fa-zindex"></i>\n' +
	'          </a>\n' +
	'        <ul class="dropdown-menu no-border-radius fadeInUp animated">\n' +
	'          <div class="col-md-12">\n' +
	'                          <div class="region region-site-map">\n' +
	'  <div id="block-menu-block-2" class="block block-menu-block footermap">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <div class="menu-block-wrapper menu-block-2 menu-name-main-menu parent-mlid-0 menu-level-1">\n' +
	'  <ul class="menu"><li class="first expanded menu-mlid-617"><a title="" class="nolink">О нас</a><ul class="menu"><li class="first leaf menu-mlid-662"><a href="/ru/content/polozhenie-o-komitete" title="">Положение о комитете</a></li>\n' +
	'<li class="leaf menu-mlid-815"><a href="/ru/content/rukovodstvo" title="">Руководство</a></li>\n' +
	'<li class="leaf menu-mlid-814"><a href="/ru/content/territorialnye-organy" title="">Структура</a></li>\n' +
	'<li class="leaf menu-mlid-819"><a href="/ru/content/strategiya" title="">Стратегия</a></li>\n' +
	'<li class="leaf has-children menu-mlid-816"><a href="/ru/section/kadrovoe-obespechenie" title="">Кадровое обеспечение</a></li>\n' +
	'<li class="leaf has-children menu-mlid-1255"><a href="/ru/section/podvedomstvennye-organizacii" title="">Подведомственные организации</a></li>\n' +
	'<li class="leaf has-children menu-mlid-785"><a href="/ru/section/informaciya-o-tekushchey-deyatelnosti" title="">Имидж госслужащего</a></li>\n' +
	'<li class="leaf has-children menu-mlid-781"><a href="/ru/section/gosudarstvennye-zakupki" title="">Государственные закупки</a></li>\n' +
	'<li class="leaf has-children menu-mlid-827"><a title="" class="nolink">Работа с населением</a></li>\n' +
	'<li class="leaf has-children menu-mlid-1063"><a href="/ru/section/press-centr" title="">Пресс-центр</a></li>\n' +
	'<li class="leaf menu-mlid-5245"><a href="/ru/content/kontakty-1-0" title="">Контакты</a></li>\n' +
	'<li class="leaf menu-mlid-7077"><a href="/ru/section/reyting-doing-business" title="">Рейтинг ГИК ВЭФ</a></li>\n' +
	'<li class="last leaf menu-mlid-6045"><a href="/ru/content/upolnomochennyy-po-etike-1" title="">Уполномоченный по этике</a></li>\n' +
	'</ul></li>\n' +
	'<li class="expanded menu-mlid-1279"><a href="/ru/section/deyatelnost" title="">Деятельность</a><ul class="menu"><li class="first leaf has-children menu-mlid-1280"><a href="/ru/section/informacionnye-sistemy" title="">Информационные системы</a></li>\n' +
	'<li class="leaf menu-mlid-1320"><a href="/ru/content/spravochniki-0" title="">Справочники</a></li>\n' +
	'<li class="leaf has-children menu-mlid-2386"><a href="/ru/section/statistika" title="">Статистика</a></li>\n' +
	'<li class="leaf has-children menu-mlid-1059"><a href="/ru/section/spiski-platelshchikov-bankrotov" title="">Списки плательщиков</a></li>\n' +
	'<li class="leaf menu-mlid-3369"><a href="/ru/section/o-realizacii-pyati-reform-100-shagov" title="">О реализации Пяти реформ "100 шагов"</a></li>\n' +
	'<li class="leaf has-children menu-mlid-2974"><a href="/ru/section/tamozhennoe-administrirovanie" title="">Таможенное администрирование</a></li>\n' +
	'<li class="leaf menu-mlid-3276"><a href="/ru/section/voprosy-protivodeystviya-korrupcii" title="">Вопросы противодействия коррупции</a></li>\n' +
	'<li class="leaf has-children menu-mlid-1318"><a href="/ru/section/vsemirnyy-bank" title="">Всемирный банк</a></li>\n' +
	'<li class="leaf has-children menu-mlid-2474"><a href="/ru/section/legalizaciya" title="">Легализация</a></li>\n' +
	'<li class="leaf has-children menu-mlid-5066"><a href="/ru/section/nalogovoe-administrirovanie" title="">Налоговое администрирование</a></li>\n' +
	'<li class="leaf has-children menu-mlid-3411"><a href="/ru/section/vto" title="">ВТО</a></li>\n' +
	'<li class="leaf has-children menu-mlid-2762"><a href="/ru/section/mezhdunarodnoe-sotrudnichestvo" title="">Международное сотрудничество</a></li>\n' +
	'<li class="leaf menu-mlid-5053"><a href="/ru/section/poryadok-raboty" title="">Порядок работы</a></li>\n' +
	'<li class="leaf menu-mlid-5567"><a href="http://kgd.gov.kz/ru/section/grazhdanam" title="">Гражданам</a></li>\n' +
	'<li class="leaf menu-mlid-5616"><a href="/ru/section/monitoring-inostrannogo-finansirovaniya" title="">Мониторинг иностранного финансирования</a></li>\n' +
	'<li class="leaf has-children menu-mlid-5366"><a href="http://kgd.gov.kz/ru/section/pravoohranitelnaya-deyatelnost" title="">Правоохранительная деятельность</a></li>\n' +
	'<li class="leaf menu-mlid-7075"><a href="/ru/section/vseobshchee-deklarirovanie" title="">Всеобщее декларирование</a></li>\n' +
	'<li class="leaf menu-mlid-8167"><a href="/ru/content/edinyy-sovokupnyy-platezh-1" title="">Единый совокупный платеж</a></li>\n' +
	'<li class="leaf has-children menu-mlid-8151"><a href="/ru/section/markirovka" title="">Маркировка</a></li>\n' +
	'<li class="leaf menu-mlid-9195"><a href="/ru/content/mezhdunarodnyy-avtomaticheskiy-obmen-1" title="">Международный автоматический обмен</a></li>\n' +
	'<li class="leaf menu-mlid-8380"><a href="/ru/content/nalogovaya-amnistiya-dlya-subektov-malogo-i-srednego-biznesa-1" title="">Налоговая амнистия</a></li>\n' +
	'<li class="leaf menu-mlid-8807"><a href="/ru/content/trehkomponentnaya-integrirovannaya-sistema-1-0" title="">Трехкомпонентная интегрированная система</a></li>\n' +
	'<li class="last leaf has-children menu-mlid-7026"><a href="/ru/section/elektronnaya-torgovlya" title="">Электронная торговля</a></li>\n' +
	'</ul></li>\n' +
	'<li class="expanded menu-mlid-840"><a href="/ru/section/zakonodatelstvo-rk" title="">Законодательство РК</a><ul class="menu"><li class="first leaf menu-mlid-841"><a href="/ru/section/zakony-rk" title="">Законы РК</a></li>\n' +
	'<li class="leaf menu-mlid-839"><a href="/ru/content/nalogovyy-kodeks-rk" title="">Налоговый кодекс РК</a></li>\n' +
	'<li class="leaf menu-mlid-1138"><a href="/ru/content/kodeks-rk-o-tamozhennom-dele-v-rk" title="">Кодекс РК «О таможенном деле в РК»</a></li>\n' +
	'<li class="leaf menu-mlid-1140"><a href="/ru/content/ugolovno-processualnyy-kodeks-rk" title="">Уголовно-процессуальный кодекс РК</a></li>\n' +
	'<li class="leaf menu-mlid-1139"><a href="/ru/content/ugolovnyy-kodeks-rk-0" title="">Уголовный Кодекс РК</a></li>\n' +
	'<li class="leaf menu-mlid-6503"><a href="/ru/content/kodeks-respubliki-kazahstan-ob-administrativnyh-pravonarusheniyah-1" title="">Кодекс РК «Об административных правонарушениях»</a></li>\n' +
	'<li class="leaf menu-mlid-1298"><a href="/ru/section/kommentariy-k-nalogovomu-kodeksu-rk" title="">Комментарий к Налоговому кодексу РК</a></li>\n' +
	'<li class="leaf has-children menu-mlid-1425"><a href="/ru/section/formy-nalogovoy-otchetnosti" title="">Формы налоговой отчетности</a></li>\n' +
	'<li class="last leaf has-children menu-mlid-1142"><a href="/ru/section/normativnye-pravovye-akty-1" title="">Нормативные правовые акты</a></li>\n' +
	'</ul></li>\n' +
	'<li class="expanded menu-mlid-3453"><a href="/ru/section/reabilitaciya-i-bankrotstvo" title="">Реабилитация и банкротство</a><ul class="menu"><li class="first leaf menu-mlid-3474"><a href="/ru/section/reestry-trebovaniy-kreditorov-utverzhdennyh-v-ramkah-zakona-respubliki-kazahstan-o" title="">Реестры требований кредиторов, утвержденных в рамках Закона Республики Казахстан "О банкротстве"</a></li>\n' +
	'<li class="leaf has-children menu-mlid-3457"><a href="/ru/section/territorialnye-organy" title="">Территориальные органы</a></li>\n' +
	'<li class="leaf menu-mlid-9228"><a href="/ru/content/voprosy-otvety-1-3" title="">Вопросы – ответы</a></li>\n' +
	'<li class="leaf menu-mlid-8474"><a href="/ru/content/edinyy-reestr-lic-uvedomleniya-kotoryh-vklyucheny-v-reestr-uvedomleniy-lic-imeyushchih-pravo" title="">Единый реестр лиц, уведомления которых включены в реестр уведомлений лиц, имеющих право осуществлять деятельность администратора</a></li>\n' +
	'<li class="leaf has-children menu-mlid-9230"><a href="/ru/content/informacionnye-materialy-1" title="">Информационные материалы</a></li>\n' +
	'<li class="leaf menu-mlid-9227"><a href="/ru/content/normativno-pravovaya-baza-1" title="">Нормативно-правовая база</a></li>\n' +
	'<li class="last leaf menu-mlid-8473"><a href="/ru/content/2019-god-1-3" title="">Реестр саморегулируемых организаций в сфере реабилитации и банкротства</a></li>\n' +
	'</ul></li>\n' +
	'<li class="leaf menu-mlid-8202"><a href="/ru/content/territoriya-nalogoplatelshchika-1" title="">ГОСУДАРСТВЕННЫЕ УСЛУГИ - ТЕРРИТОРИЯ НАЛОГОПЛАТЕЛЬЩИКА</a></li>\n' +
	'<li class="expanded menu-mlid-2463"><a href="/ru/section/gik-vef-doing-business" title="">Рейтинг «Doing Business»</a><ul class="menu"><li class="first leaf menu-mlid-7079"><a href="/ru/section/reyting-doing-business" title="">Рейтинг «Doing Business»</a></li>\n' +
	'<li class="last leaf menu-mlid-7078"><a href="/ru/section/reyting-gik-vef" title="">Рейтинг ГИК ВЭФ</a></li>\n' +
	'</ul></li>\n' +
	'<li class="leaf menu-mlid-8776"><a href="http://kgd.gov.kz/ru/faq" title="">Часто задаваемые вопросы</a></li>\n' +
	'<li class="expanded menu-mlid-5079"><a href="/ru/section/elektronnye-scheta-faktury" title="">ИС ЭСФ</a><ul class="menu"><li class="first leaf has-children menu-mlid-7124"><a href="/ru/section/api-interfeys" title="">API - интерфейс</a></li>\n' +
	'<li class="leaf menu-mlid-7138"><a href="/ru/content/virtualnyy-sklad-1" title="">Виртуальный склад</a></li>\n' +
	'<li class="leaf menu-mlid-10073"><a href="/ru/content/dokumenty-po-esf-1" title="">Документы по ИС ЭСФ</a></li>\n' +
	'<li class="leaf menu-mlid-5083"><a href="/ru/content/zakonodatelstvo-1" title="">Законодательство</a></li>\n' +
	'<li class="leaf menu-mlid-5081"><a href="/ru/content/informacionnye-materialy-po-sisteme-1" title="">Информационные материалы по системе</a></li>\n' +
	'<li class="leaf menu-mlid-8946"><a href="/ru/content/pilotnyy-proekt-1" title="">Пилотный проект</a></li>\n' +
	'<li class="leaf menu-mlid-5084"><a href="/ru/content/programmnoe-obespechenie-1-0" title="">Программное обеспечение</a></li>\n' +
	'<li class="leaf menu-mlid-5085"><a href="/ru/content/sluzhba-podderzhki-esf-1" title="">Служба поддержки ИС ЭСФ</a></li>\n' +
	'<li class="leaf has-children menu-mlid-9222"><a href="/ru/content/soprovoditelnaya-nakladnaya-na-tovary-1" title="">Сопроводительная накладная на товары</a></li>\n' +
	'<li class="last leaf menu-mlid-5082"><a href="/ru/content/testovyy-stend-esf-1" title="">Тестовый стенд ИС ЭСФ</a></li>\n' +
	'</ul></li>\n' +
	'<li class="leaf menu-mlid-7736"><a href="/ru/content/predvaritelnye-resheniya-o-klassifikacii-tovarov-1" title="">Предварительные решения о классификации товаров</a></li>\n' +
	'<li class="expanded menu-mlid-6796"><a href="/ru/section/digitalization" title="">ЦИФРОВИЗАЦИЯ</a><ul class="menu"><li class="first leaf menu-mlid-6798"><a href="/ru/content/poslanie-prezidenta-respubliki-kazahstan-nnazarbaeva-narodu-kazahstana-1" title="">Послание Президента Республики Казахстан</a></li>\n' +
	'<li class="leaf menu-mlid-10190"><a href="/ru/content/organizacionnaya-struktura-upravleniya-1" title="">ОРГАНИЗАЦИОННАЯ СТРУКТУРА УПРАВЛЕНИЯ</a></li>\n' +
	'<li class="leaf menu-mlid-10192"><a href="/ru/content/proekty-1-0" title="">ПРОЕКТЫ</a></li>\n' +
	'<li class="last leaf menu-mlid-10191"><a href="/ru/content/reglament-realizacii-proektov-1" title="">РЕГЛАМЕНТ РЕАЛИЗАЦИИ ПРОЕКТОВ</a></li>\n' +
	'</ul></li>\n' +
	'<li class="leaf menu-mlid-8194"><a href="/ru/content/cenovaya-informaciya-1" title="">Ценовая информация по товарам</a></li>\n' +
	'<li class="last leaf menu-mlid-10193"><a href="/ru/covid-19" title="" class="smartmenu-covid">МЕРЫ ПОДДЕРЖКИ НАЛОГОПЛАТЕЛЬЩИКОВ В ПЕРИОД ЧП (COVID-19)</a></li>\n' +
	'</ul></div>\n' +
	'  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'                      </div>\n' +
	'        </ul>\n' +
	'      </li></ul>\n' +
	'    </div>\n' +
	'  </header>\n' +
	'\n' +
	'  <div class="container">\n' +
	'    <div class="m-t"></div>\n' +
	'      <div class="cur-vt-none scroll-to-fixed-fixed" id="fixed-header" style="z-index: 1000; position: fixed; top: 0px; margin-left: 0px; width: 696.2px; left: 40px;">\n' +
	'        <div id="breadcrumbs"><h2 class="element-invisible">Вы здесь</h2><div class="breadcrumb" xmlns:v="http://rdf.data-vocabulary.org/#"><span class="inline odd first" typeof="v:Breadcrumb"><a href="http://kgd.gov.kz/ru" rel="v:url" property="v:title">Главная</a></span> <span class="delimiter">»</span> <span class="inline even" typeof="v:Breadcrumb"><a href="http://kgd.gov.kz/ru/all/services" rel="v:url" property="v:title">Электронные сервисы</a></span> <span class="delimiter">»</span> <span class="inline odd last" typeof="v:Breadcrumb"><a href="http://kgd.gov.kz/ru/all/services#331" rel="v:url" property="v:title">Помощь бизнесу</a></span></div></div>                <h4 class="page-title ">Сведения об отсутствии (наличии) задолженности, учет по которым ведется в органах государственных доходов</h4>              </div><div style="display: block; width: 720px; height: 111px; float: none;"></div>\n' +
	'    <div class="row">\n' +
	'      <div class="col-md-9">\n' +
	'          <section id="content" role="main" class="clearfix">\n' +
	'            <div id="container">\n' +
	'              <div id="fixed-div" class="scroll">\n' +
	'                <a id="print" class="btn btn-icon m-t-xs btn-lg btn-printer m-t-lg"><i class="fa fa-print fa-x1-2"></i></a>\n' +
	'                <a target="_blank" class="btn btn-icon m-t-xs btn-lg btn-facebook" href="http://facebook.com//sharer.php?u=http://kgd.gov.kz/ru/app/culs-taxarrear-search-web&amp;t=Сведения об отсутствии (наличии) задолженности, учет по которым ведется в органах государственных доходов"><i class="fa fa-facebook fa-x1-2"></i></a>\n' +
	'                <a target="_blank" class="btn btn-icon m-t-xs btn-lg btn-twitter" href="http://twitter.com/intent/tweet?url=http://kgd.gov.kz/ru/app/culs-taxarrear-search-web&amp;text=Сведения об отсутствии (наличии) задолженности, учет по которым ведется в органах государственных доходов"><i class="fa fa-twitter fa-x1-2"></i></a>\n' +
	'                <a target="_blank" class="btn btn-icon m-t-xs btn-lg btn-google" href="https://plus.google.com/share?url=http://kgd.gov.kz/ru/app/culs-taxarrear-search-web"><i class="fa fa-google-plus fa-x1-2"></i></a>\n' +
	'                <a id="fixed-down" class="btn btn-icon m-t-xs btn-lg btn-totop" style=""><i class="fa fa-angle-up fa-2x"></i></a>\n' +
	'              </div>\n' +
	'            </div>\n' +
	'            <div id="content-wrap">\n' +
	'                                                        <div class="region region-content">\n' +
	'  <div id="block-system-main" class="block block-system">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    \n' +
	'<div id="load-content"><!--<meta charset="UTF-8">-->\n' +
	'\n' +
	'<script src="/apps/services/culs-taxarrear-search-web/resources/js/translation.js"></script>\n' +
	'<script src="/apps/services/culs-taxarrear-search-web/resources/js/tablebuilder.js"></script>\n' +
	'<script src="/apps/services/culs-taxarrear-search-web/resources/js/sender.js"></script>\n' +
	'<script src="/apps/services/culs-taxarrear-search-web/resources/js/pay.js"></script>\n' +
	'<link type="text/css" rel="stylesheet" href="/apps/services/culs-taxarrear-search-web/resources/prodlib/css/culs.css" media="all">\n' +
	'\n' +
	'<style>\n' +
	'    #content-wrap a.btn {text-decoration: none !important; display: block;}\n' +
	'    #result .form-control {width: 100%; height: 30px;}\n' +
	'</style>\n' +
	'<!--<div class="container">-->\n' +
	'<div>\n' +
	'    <div id="errorAlerts">\n' +
	'        <div id="app-alert" class="messages error hide"></div>\n' +
	'    </div>\n' +
	'    <div id="culsform" style="margin-bottom: 20px;">\n' +
	'        <form method="post" id="mainform" class="pure-form" action="/apps/services/culs-taxarrear-search-web/resources/rest/search">\n' +
	'            <div>\n' +
	'                <div>\n' +
	'                    <div id="iinBinG" class="form-item">\n' +
	'                        <label for="iinBin" id="iinBinLabelId">ИИН/БИН</label>\n' +
	'                        <input type="text" class="form-control form-text" name="iinBin" id="iinBin" maxlength="12" value="">\n' +
	'                    </div>\n' +
	'                    <div id="captcha-culs-div" class="form-item">\n' +
	'                        <img src="/apps/services/CaptchaWeb/generate?uid=e6f939f8-a4ef-4de4-b1c7-6079c10b63c7&amp;t=e951f021-3d45-4453-869d-e211d225b5d0" id="captcha-img">\n' +
	'                        <input type="hidden" id="captcha-id" name="captcha-id" value="e6f939f8-a4ef-4de4-b1c7-6079c10b63c7">\n' +
	'                        <div style="margin-top: 5px">\n' +
	'                            <a id="reloadImg" href="#" onclick="reloadCaptcha(); return false;">Обновить код на картинке</a>\n' +
	'                        </div>\n' +
	'                        <div class="form-item m-b-xxs">\n' +
	'                            <label for="captcha-user-value" id="captchaLabel">Какой код на картинке?</label>\n' +
	'                            <input type="text" class="form-control form-text" name="captcha-user-value" id="captcha-user-value">\n' +
	'                            <span></span>\n' +
	'                        </div>\n' +
	'                    </div>\n' +
	'                    <span id="systemMessage" class="text-danger"></span>\n' +
	'\n' +
	'                    <input id="btnGetResult" type="button" class="btn btn-index" value="Получить результат">\n' +
	'                    <input id="btnClearField" type="button" class="btn btn-default" value="Очистить">\n' +
	'                </div>\n' +
	'            </div>\n' +
	'        </form>\n' +
	'    </div>\n' +
	'    <div id="result" style="">\n' +
	'        <div class="panel-heading">\n' +
	'            <table id="table0" class="borderless">\n' +
	'                <tbody><tr>\n' +
	'                    <td id="result.table.1.row.1">Наименование налогоплательщика</td>\n' +
	'                    <td class="font-bold" id="table1_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td>\n' +
	'                </tr>\n' +
	'                <tr>\n' +
	'                    <td id="result.table.1.row.2">ИИН/БИН налогоплательщика</td>\n' +
	'                    <td class="font-bold" id="table1_iinBin">920440000372</td>\n' +
	'                </tr>\n' +
	'                <tr>\n' +
	'                    <td id="result.table.1.row.3">Всего задолженности (тенге)</td>\n' +
	'                    <td class="font-bold" id="table1_totalArrear">0.00</td>\n' +
	'                </tr>\n' +
	'            </tbody></table>\n' +
	'        </div>\n' +
	'\n' +
	'        <div class="panel panel-default">\n' +
	'            <table id="table1" class="table borderless m-t">\n' +
	'                <tbody><tr>\n' +
	'                    <td id="result.table.2.row.1">Итого задолженности в бюджет</td>\n' +
	'                    <td class="font-bold tdWidth" id="table2_totalTaxArrear">0.00</td>\n' +
	'                </tr>\n' +
	'                <tr>\n' +
	'                    <td id="result.table.2.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\n' +
	'                    <td class="font-bold tdWidth" id="table2_pensionContributionArrear">0.00</td>\n' +
	'                </tr>\n' +
	'                <tr>\n' +
	'                    <td id="result.table.2.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\n' +
	'                    <td class="font-bold tdWidth" id="table2_socialHealthInsuranceArrear">0.00</td>\n' +
	'                </tr>\n' +
	'                <tr>\n' +
	'                    <td id="result.table.2.row.3">Задолженность по социальным отчислениям</td>\n' +
	'                    <td class="font-bold tdWidth" id="table2_socialContributionArrear">0.00</td>\n' +
	'                </tr>\n' +
	'            </tbody></table>\n' +
	'        </div>\n' +
	'\n' +
	'        <div id="taxorginfo_cyclic_table"><span id="result.header.2" class="text-info">Таблица задолженностей по органам государственных доходов</span><div id="TableContener_0" class="m-t"><div id="taxOrgPanel-0" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение "Управление государственных доходов по Есильскому району Департамента государственных доходов по городу Нур-Султан Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>620501</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-0">Подробнее...</button><div id="Table-0-Content" class="m-t-sm"><div class="collapse" id="tableInfo-0"><table id="table4_0" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-0-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Branch of GRATA International in Astana</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">190441900132</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-0-0">Подробнее...</button><div class="collapse" id="tableInfo-0-0"><div id="Table2-0-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_0_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="903101">903101 Поступления обязательных пенсионных профессиональных взносов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="904101">904101 Поступления отчислений и взносов на обязательное социальное медицинское страхование</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="901101">901101 Поступления в пенсионный фонд 901101</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="902101">902101 Поступления социальных отчислений</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div><div id="TableContener_1" class="m-t"><div id="taxOrgPanel-1" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение "Управление государственных доходов по Алмалинскому району Департамента государственных доходов по городу Алматы Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>600701</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-1">Подробнее...</button><div id="Table-1-Content" class="m-t-sm"><div class="collapse" id="tableInfo-1"><table id="table4_1" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-1-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">920440000372</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-1-0">Подробнее...</button><div class="collapse" id="tableInfo-1-0"><div id="Table2-1-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_1_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="104302">104302 Земельный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105101">105101 Hалог на добавленную стоимость на произведенные товары, выполненные работы и оказанные услуги на территории Республики Казахстан</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104401">104401 Hалог на транспортные средства с юридических лиц</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104101">104101 Налог на имущество юридических лиц и индивидуальных предпринимателей</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101202">101202 Индивидуальный подоходный налог с доходов, не облагаемых у источника выплаты</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105316">105316 Плата за эмиссии в окружающую среду</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div><div id="TableContener_2" class="m-t"><div id="taxOrgPanel-2" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение "Управление государственных доходов по Сарыаркинскому району Департамента государственных доходов по городу Астане Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>620301</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-2">Подробнее...</button><div id="Table-2-Content" class="m-t-sm"><div class="collapse" id="tableInfo-2"><table id="table4_2" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-2-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">920440000372</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-2-0">Подробнее...</button><div class="collapse" id="tableInfo-2-0"><div id="Table2-2-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_2_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="105424">105424 Плата за размещение наружной (визуальной) рекламы на открытом пространстве за пределами помещений в городе республиканского значения, столице</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_1"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Филиал товарищества с ограниченной ответственностью «Юридическая фирма «GRATA» в городе Астана</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">980241001736</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-2-1">Подробнее...</button><div class="collapse" id="tableInfo-2-1"><div id="Table2-2-1-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_2_1">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="105424">105424 Плата за размещение наружной (визуальной) рекламы на открытом пространстве за пределами помещений в городе республиканского значения, столице</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="901101">901101 Поступления в пенсионный фонд 901101</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="902101">902101 Поступления социальных отчислений</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101111">101111 Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="903101">903101 Поступления обязательных пенсионных профессиональных взносов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="904101">904101 Поступления отчислений и взносов на обязательное социальное медицинское страхование</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div><div id="TableContener_3" class="m-t"><div id="taxOrgPanel-3" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение "Управление государственных доходов по Бостандыкскому району Департамента государственных доходов по городу Алматы Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>600401</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-3">Подробнее...</button><div id="Table-3-Content" class="m-t-sm"><div class="collapse" id="tableInfo-3"><table id="table4_3" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-3-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">920440000372</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-3-0">Подробнее...</button><div class="collapse" id="tableInfo-3-0"><div id="Table2-3-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_3_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="105104">105104 Налог на добавленную стоимость за нерезидента</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105316">105316 Плата за эмиссии в окружающую среду</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="204162">204162 Административные штрафы, пени, санкции, взыскания, налагаемые Комитетом государственных доходов Министерства финансов Республики Казахстан, его территориальными органами финансируемые из республиканского бюджета, за исключением поступлений от организаций нефтяного сектора</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104401">104401 Hалог на транспортные средства с юридических лиц</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="107109">107109 Прочие налоговые поступления в республиканский бюджет</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="901101">901101 Поступления в пенсионный фонд 901101</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105101">105101 Hалог на добавленную стоимость на произведенные товары, выполненные работы и оказанные услуги на территории Республики Казахстан</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="903101">903101 Поступления обязательных пенсионных профессиональных взносов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104302">104302 Земельный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="904101">904101 Поступления отчислений и взносов на обязательное социальное медицинское страхование</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105115">105115 Налог на добавленную стоимость на товары, импортированные с территории государств-членов ЕАЭС</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101111">101111 Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104101">104101 Налог на имущество юридических лиц и индивидуальных предпринимателей</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105102">105102 Hалог на добавленную стоимость на товары, импортируемые на территорию Республики Казахстан, кроме налога на добавленную стоимость на товары, импортируемые с территории Российской Федерации и Республики Беларусь</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="106201">106201 Таможенные сборы, уплачиваемые в соответствии с таможенным законодательством Республики Казахстан.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="106101">106101 Ввозные таможенные пошлины (иные пошлины, налоги и сборы, имеющие эквивалентное действие), уплаченные в соответствии с Договором о Евразийском экономическом союзе</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="902101">902101 Поступления социальных отчислений</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div><div id="TableContener_4" class="m-t"><div id="taxOrgPanel-4" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение "Управление государственных доходов по городу Атырау Департамента государственных доходов по Атырауской области Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>151001</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-4">Подробнее...</button><div id="Table-4-Content" class="m-t-sm"><div class="collapse" id="tableInfo-4"><table id="table4_4" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-4-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Филиал товарищества с ограниченной ответственностью "Юридическая фирма "GRАТА" в Атырау</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">971241001628</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-4-0">Подробнее...</button><div class="collapse" id="tableInfo-4-0"><div id="Table2-4-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_4_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="902101">902101 Поступления социальных отчислений</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104401">104401 Hалог на транспортные средства с юридических лиц</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104101">104101 Налог на имущество юридических лиц и индивидуальных предпринимателей</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="901101">901101 Поступления в пенсионный фонд 901101</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="904101">904101 Поступления отчислений и взносов на обязательное социальное медицинское страхование</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="104302">104302 Земельный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101111">101111 Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="903101">903101 Поступления обязательных пенсионных профессиональных взносов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_1"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">920440000372</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-4-1">Подробнее...</button><div class="collapse" id="tableInfo-4-1"><div id="Table2-4-1-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_4_1">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="104302">104302 Земельный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div><div id="TableContener_5" class="m-t"><div id="taxOrgPanel-5" class="panel panel-default"><div class="panel-heading font-bold">Орган государственных доходов Республиканское государственное учреждение " Управление государственных доходов по городу Актау Департамента государственных доходов по Мангистауской области Комитета государственных доходов Министерства финансов Республики Казахстан" Код ОГД <strong>430601</strong></div><div class="panel-body"><div id="result.table.3.row.2">По состоянию на 2020-05-09</div><div id="result.table.3.row.3">Всего задолженности: 0.00</div></div><div class="panel-footer bg-light lter"><button class="btn btn-sm btn-default m-t-sm" data-toggle="collapse" data-target="#tableInfo-5">Подробнее...</button><div id="Table-5-Content" class="m-t-sm"><div class="collapse" id="tableInfo-5"><table id="table4_5" class="table table-bordered table-hover"><tbody><tr>\t<td id="result.table.4.row.1">Итого задолженности в бюджет</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_totalTaxArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.2">Задолженность по обязательным пенсионным взносам, обязательным профессиональным пенсионным взносам</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_pensionContributionArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.med">Задолженность по отчислениям и (или) взносам на обязательное социальное медицинское страхование</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialHealthInsuranceArrear">0.00</td></tr><tr>\t<td id="result.table.4.row.3">Задолженность по социальным отчислениям</td>\t<td class="tdWidth font-bold" id="taxOrgInfo_socialContributionArrear">0.00</td></tr></tbody></table><div id="Table3-5-Content"><span id="result.header.3" class="text-info">Таблица задолженностей по налогоплательщику и его структурным подразделениям</span><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_0"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Филиал товарищества с ограниченной ответственностью "Юридическая фирма "GRATA" в городе Актау</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">010241002298</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-5-0">Подробнее...</button><div class="collapse" id="tableInfo-5-0"><div id="Table2-5-0-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_5_0">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="904101">904101 Поступления отчислений и взносов на обязательное социальное медицинское страхование</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="105420">105420 Плата за размещ. наружной (визуальной) рекламы на объектах стац. размещ. рекл.в полосе отвода автом.дорог общего польз. обл. знач, за искл. платы за размещ. нар. (виз.) рекл. на объек. стац. размещ. рекл. в пол. отв. автом. ДОП. обл. зн., проход. через терр. гор. рай. зн., сел, поселков, сел. округов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="902101">902101 Поступления социальных отчислений</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="103101">103101 Социальный налог</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="901101">901101 Поступления в пенсионный фонд 901101</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101111">101111 Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'<tr><td id="903101">903101 Поступления обязательных пенсионных профессиональных взносов</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div><div class="wrapper m-t"><table class="table table-bordered table-hover" id="table5_1"><tbody><tr><td id="result.table.5.row.1">Наименование налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_name">Товарищество с ограниченной ответственностью " Юридическая фирма "GRATA"</td></tr><tr><td id="result.table.5.row.2">ИИН/БИН налогоплательщика:</td><td class="font-bold" id="taxPayerInfo_iinBin">920440000372</td></tr><tr><td id="result.table.5.row.3">Всего задолженности:</td><td class="font-bold" id="taxPayerInfo_totalArrear">0.00</td></tr></tbody></table><button class="btn btn-sm btn-default" data-toggle="collapse" data-target="#tableInfo-5-1">Подробнее...</button><div class="collapse" id="tableInfo-5-1"><div id="Table2-5-1-Content"><table class="table table-bordered table-hover" style="font-size:12px;" id="table6_5_1">    <thead><tr>        <th id="result.table.6.column.1.label" style="width:20%">КБК</th>        <th id="result.table.6.column.2.label" style="width:15%">Задолженность по платежам, учет по которым ведется в органах государственных доходов</th>        <th id="result.table.6.column.3.label" style="width:15%">Задолженность по сумме пени</th>        <th id="result.table.6.column.4.label" style="width:15%">Задолженность по сумме процентов</th>        <th id="result.table.6.column.5.label" style="width:15%">Задолженность по сумме штрафа</th>        <th id="result.table.6.column.6.label" style="width:15%">Всего задолженности</th>    </tr></thead><tbody><tr><td id="101201">101201 Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td><td>0.00</td></tr>\n' +
	'</tbody></table></div></div></div></div></div></div></div></div></div></div>\n' +
	'\n' +
	'        <form method="POST" action="/apps/services/culs-taxarrear-search-web/rest/pdf?lang=ru" id="downloadForm">\n' +
	'            <input type="hidden" name="jsonForPdf" id="jsonForPdf" value="{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;sendTime&quot;:1589110519000,&quot;taxOrgInfo&quot;:[{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot;Управление государственных доходов по Есильскому району Департамента государственных доходов по городу Нур-Султан Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Нұр-Сұлтан қаласы бойынша Мемлекеттік кірістер департаментінің Есіл ауданы бойынша Мемлекеттік кірістер басқармасы\\&quot; республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;620501&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Branch of GRATA International in Astana&quot;,&quot;nameKk&quot;:&quot;Branch of GRATA International in Astana&quot;,&quot;iinBin&quot;:&quot;190441900132&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;903101&quot;,&quot;bccNameRu&quot;:&quot;Поступления обязательных пенсионных профессиональных взносов&quot;,&quot;bccNameKz&quot;:&quot;Міндетті кәсіптік зейнетақы жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;904101&quot;,&quot;bccNameRu&quot;:&quot;Поступления отчислений и взносов на обязательное социальное медицинское страхование&quot;,&quot;bccNameKz&quot;:&quot;Міндетті әлеуметтік медициналық сақтандыру аударымдар және  жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;901101&quot;,&quot;bccNameRu&quot;:&quot;Поступления в пенсионный фонд 901101&quot;,&quot;bccNameKz&quot;:&quot;Міндетті зейнетақы жарналары&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;902101&quot;,&quot;bccNameRu&quot;:&quot;Поступления социальных отчислений&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтiк аударымдар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]},{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot;Управление государственных доходов по Алмалинскому району Департамента государственных доходов по городу Алматы Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Алматы қаласы бойынша Мемлекеттік кірістер департаментінің Алмалы ауданы бойынша Мемлекеттік кірістер басқармасы\\&quot; республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;600701&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;104302&quot;,&quot;bccNameRu&quot;:&quot;Земельный налог&quot;,&quot;bccNameKz&quot;:&quot;Елдi мекендер жерлерiне алынатын жер салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105101&quot;,&quot;bccNameRu&quot;:&quot;Hалог на добавленную стоимость на произведенные товары, выполненные работы и оказанные услуги на территории Республики Казахстан&quot;,&quot;bccNameKz&quot;:&quot;Қазақстан Республикасының аумағында өндiрiлген тауарларға, орындалған жұмыстарға және көрсетілген қызметтерге салынатын қосылған құн салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104401&quot;,&quot;bccNameRu&quot;:&quot;Hалог на транспортные средства с юридических лиц&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың көлiк құралдарына салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104101&quot;,&quot;bccNameRu&quot;:&quot;Налог на имущество юридических лиц и индивидуальных предпринимателей&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың және жеке кәсіпкерлердің мүлкіне салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101202&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, не облагаемых у источника выплаты&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынбайтын табыстардан ұсталатын жеке табыс салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105316&quot;,&quot;bccNameRu&quot;:&quot;Плата за эмиссии в окружающую среду&quot;,&quot;bccNameKz&quot;:&quot;Қоршаған ортаға эмиссия үшін төленетін төлемақы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]},{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot;Управление государственных доходов по Сарыаркинскому району Департамента государственных доходов по городу Астане Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Астана қаласы бойынша Мемлекеттік кірістер департаментінің Сарыарқа ауданы бойынша Мемлекеттік кірістер басқармасы\\&quot; республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;620301&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;105424&quot;,&quot;bccNameRu&quot;:&quot;Плата за размещение наружной (визуальной) рекламы на открытом пространстве за пределами помещений в городе республиканского значения, столице&quot;,&quot;bccNameKz&quot;:&quot;Сыртқы (көрнекі) жарнаманы республикалық маңызы бар қаладағы, астанадағы үй-жайлардың шегінен тыс ашық кеңістікте орналастырғаны үшін төлемақы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]},{&quot;nameRu&quot;:&quot;Филиал товарищества с ограниченной ответственностью «Юридическая фирма «GRATA» в городе Астана&quot;,&quot;nameKk&quot;:&quot;Астана қаласындағы «GRATA» заң фирмасы» жауапкершілігі шектеулі серіктестігінің филиалы&quot;,&quot;iinBin&quot;:&quot;980241001736&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;105424&quot;,&quot;bccNameRu&quot;:&quot;Плата за размещение наружной (визуальной) рекламы на открытом пространстве за пределами помещений в городе республиканского значения, столице&quot;,&quot;bccNameKz&quot;:&quot;Сыртқы (көрнекі) жарнаманы республикалық маңызы бар қаладағы, астанадағы үй-жайлардың шегінен тыс ашық кеңістікте орналастырғаны үшін төлемақы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;901101&quot;,&quot;bccNameRu&quot;:&quot;Поступления в пенсионный фонд 901101&quot;,&quot;bccNameKz&quot;:&quot;Міндетті зейнетақы жарналары&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;902101&quot;,&quot;bccNameRu&quot;:&quot;Поступления социальных отчислений&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтiк аударымдар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101111&quot;,&quot;bccNameRu&quot;:&quot;Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора&quot;,&quot;bccNameKz&quot;:&quot;Ірі кәсіпкерлік субъектілерінен және мұнай секторы ұйымдарынан түсетін түсімдерді қоспағанда, заңды тұлғалардан алынатын корпоративтік табыс салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;903101&quot;,&quot;bccNameRu&quot;:&quot;Поступления обязательных пенсионных профессиональных взносов&quot;,&quot;bccNameKz&quot;:&quot;Міндетті кәсіптік зейнетақы жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;904101&quot;,&quot;bccNameRu&quot;:&quot;Поступления отчислений и взносов на обязательное социальное медицинское страхование&quot;,&quot;bccNameKz&quot;:&quot;Міндетті әлеуметтік медициналық сақтандыру аударымдар және  жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]},{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot;Управление государственных доходов по Бостандыкскому району Департамента государственных доходов по городу Алматы Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Алматы қаласы бойынша Мемлекеттік кірістер департаментінің Бостандық ауданы бойынша Мемлекеттік кірістер басқармасы\\&quot;республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;600401&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;105104&quot;,&quot;bccNameRu&quot;:&quot;Налог на добавленную стоимость за нерезидента&quot;,&quot;bccNameKz&quot;:&quot;Резидент емес үшін қосылған құн салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105316&quot;,&quot;bccNameRu&quot;:&quot;Плата за эмиссии в окружающую среду&quot;,&quot;bccNameKz&quot;:&quot;Қоршаған ортаға эмиссия үшін төленетін төлемақы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;204162&quot;,&quot;bccNameRu&quot;:&quot;Административные штрафы, пени, санкции, взыскания, налагаемые Комитетом государственных доходов Министерства финансов Республики Казахстан, его территориальными органами финансируемые из республиканского бюджета, за исключением поступлений от организаций нефтяного сектора&quot;,&quot;bccNameKz&quot;:&quot;Мұнай секторы ұйымдарынан түсетін түсімдерді қоспағанда, Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті, республикалық бюджеттен қаржыландырылатын оның аумақтық бөлімшелері салатын әкiмшiлiк айыппұлдар, өсімпұлдар, санкциялар, өндіріп алулар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104401&quot;,&quot;bccNameRu&quot;:&quot;Hалог на транспортные средства с юридических лиц&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың көлiк құралдарына салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;107109&quot;,&quot;bccNameRu&quot;:&quot;Прочие налоговые поступления в республиканский бюджет&quot;,&quot;bccNameKz&quot;:&quot;Республикалық бюджетке түсетін өзге де салық түсiмдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;901101&quot;,&quot;bccNameRu&quot;:&quot;Поступления в пенсионный фонд 901101&quot;,&quot;bccNameKz&quot;:&quot;Міндетті зейнетақы жарналары&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105101&quot;,&quot;bccNameRu&quot;:&quot;Hалог на добавленную стоимость на произведенные товары, выполненные работы и оказанные услуги на территории Республики Казахстан&quot;,&quot;bccNameKz&quot;:&quot;Қазақстан Республикасының аумағында өндiрiлген тауарларға, орындалған жұмыстарға және көрсетілген қызметтерге салынатын қосылған құн салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;903101&quot;,&quot;bccNameRu&quot;:&quot;Поступления обязательных пенсионных профессиональных взносов&quot;,&quot;bccNameKz&quot;:&quot;Міндетті кәсіптік зейнетақы жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104302&quot;,&quot;bccNameRu&quot;:&quot;Земельный налог&quot;,&quot;bccNameKz&quot;:&quot;Елдi мекендер жерлерiне алынатын жер салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;904101&quot;,&quot;bccNameRu&quot;:&quot;Поступления отчислений и взносов на обязательное социальное медицинское страхование&quot;,&quot;bccNameKz&quot;:&quot;Міндетті әлеуметтік медициналық сақтандыру аударымдар және  жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105115&quot;,&quot;bccNameRu&quot;:&quot;Налог на добавленную стоимость на товары, импортированные с территории государств-членов ЕАЭС&quot;,&quot;bccNameKz&quot;:&quot;ЕАЭО мүше мемлекеттер аумағынан импортталған тауарларға  қосылған құн салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101111&quot;,&quot;bccNameRu&quot;:&quot;Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора&quot;,&quot;bccNameKz&quot;:&quot;Ірі кәсіпкерлік субъектілерінен және мұнай секторы ұйымдарынан түсетін түсімдерді қоспағанда, заңды тұлғалардан алынатын корпоративтік табыс салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104101&quot;,&quot;bccNameRu&quot;:&quot;Налог на имущество юридических лиц и индивидуальных предпринимателей&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың және жеке кәсіпкерлердің мүлкіне салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105102&quot;,&quot;bccNameRu&quot;:&quot;Hалог на добавленную стоимость на товары, импортируемые на территорию Республики Казахстан, кроме налога на добавленную стоимость на товары, импортируемые с территории Российской Федерации и Республики Беларусь&quot;,&quot;bccNameKz&quot;:&quot;Ресей Федерациясының және Беларусь Республикасының аумағынан импортталатын тауарларға салынатын қосылған құн салығынан басқа, Қазақстан Республикасының аумағына импортталатын тауарларға салынатын қосылған құн салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;106201&quot;,&quot;bccNameRu&quot;:&quot;Таможенные сборы, уплачиваемые в соответствии с таможенным законодательством Республики Казахстан.&quot;,&quot;bccNameKz&quot;:&quot;Қазақстан Республикасының кеден заңнамасына сәйкес төленетін кедендік алымдар.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;106101&quot;,&quot;bccNameRu&quot;:&quot;Ввозные таможенные пошлины (иные пошлины, налоги и сборы, имеющие эквивалентное действие), уплаченные в соответствии с Договором о Евразийском экономическом союзе&quot;,&quot;bccNameKz&quot;:&quot;Еуразиялық экономикалық одақ туралы шартқа сәйкес төленген әкелінетін кедендік баждары (баламалы қолданылатын өзге де баждар, салықтар мен алымдар)&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;902101&quot;,&quot;bccNameRu&quot;:&quot;Поступления социальных отчислений&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтiк аударымдар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]},{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot;Управление государственных доходов по городу Атырау Департамента государственных доходов по Атырауской области Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Атырау облысы бойынша Мемлекеттік кірістер департаментінің Атырау қаласы бойынша Мемлекеттік кірістер басқармасы\\&quot; республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;151001&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Филиал товарищества с ограниченной ответственностью \\&quot;Юридическая фирма \\&quot;GRАТА\\&quot; в Атырау&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRАТА\\&quot; заң фирмасы\\&quot; жауапкершілігі шектеулі серіктестігінің Атырау филиалы&quot;,&quot;iinBin&quot;:&quot;971241001628&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;902101&quot;,&quot;bccNameRu&quot;:&quot;Поступления социальных отчислений&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтiк аударымдар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104401&quot;,&quot;bccNameRu&quot;:&quot;Hалог на транспортные средства с юридических лиц&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың көлiк құралдарына салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104101&quot;,&quot;bccNameRu&quot;:&quot;Налог на имущество юридических лиц и индивидуальных предпринимателей&quot;,&quot;bccNameKz&quot;:&quot;Заңды тұлғалардың және жеке кәсіпкерлердің мүлкіне салынатын салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;901101&quot;,&quot;bccNameRu&quot;:&quot;Поступления в пенсионный фонд 901101&quot;,&quot;bccNameKz&quot;:&quot;Міндетті зейнетақы жарналары&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;904101&quot;,&quot;bccNameRu&quot;:&quot;Поступления отчислений и взносов на обязательное социальное медицинское страхование&quot;,&quot;bccNameKz&quot;:&quot;Міндетті әлеуметтік медициналық сақтандыру аударымдар және  жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;104302&quot;,&quot;bccNameRu&quot;:&quot;Земельный налог&quot;,&quot;bccNameKz&quot;:&quot;Елдi мекендер жерлерiне алынатын жер салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101111&quot;,&quot;bccNameRu&quot;:&quot;Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора&quot;,&quot;bccNameKz&quot;:&quot;Ірі кәсіпкерлік субъектілерінен және мұнай секторы ұйымдарынан түсетін түсімдерді қоспағанда, заңды тұлғалардан алынатын корпоративтік табыс салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;903101&quot;,&quot;bccNameRu&quot;:&quot;Поступления обязательных пенсионных профессиональных взносов&quot;,&quot;bccNameKz&quot;:&quot;Міндетті кәсіптік зейнетақы жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]},{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;104302&quot;,&quot;bccNameRu&quot;:&quot;Земельный налог&quot;,&quot;bccNameKz&quot;:&quot;Елдi мекендер жерлерiне алынатын жер салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]},{&quot;nameRu&quot;:&quot;Республиканское государственное учреждение \\&quot; Управление государственных доходов по городу Актау Департамента государственных доходов по Мангистауской области Комитета государственных доходов Министерства финансов Республики Казахстан\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;Қазақстан Республикасы Қаржы министрлігінің Мемлекеттік кірістер комитеті Маңғыстау облысы бойынша Мемлекеттік кірістер департаментінің Ақтау қаласы бойынша Мемлекеттік кірістер басқармасы\\&quot; республикалық мемлекеттік мекемесі&quot;,&quot;charCode&quot;:&quot;430601&quot;,&quot;reportAcrualDate&quot;:1589047200000,&quot;totalArrear&quot;:0,&quot;totalTaxArrear&quot;:0,&quot;pensionContributionArrear&quot;:0,&quot;socialContributionArrear&quot;:0,&quot;socialHealthInsuranceArrear&quot;:0,&quot;appealledAmount&quot;:null,&quot;modifiedTermsAmount&quot;:null,&quot;rehabilitaionProcedureAmount&quot;:null,&quot;taxPayerInfo&quot;:[{&quot;nameRu&quot;:&quot;Филиал товарищества с ограниченной ответственностью \\&quot;Юридическая фирма \\&quot;GRATA\\&quot; в городе Актау&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заңды фирмасы\\&quot; жауапкершілігі шектеулі серіктестігінің Ақтау қаласындағы филиалы&quot;,&quot;iinBin&quot;:&quot;010241002298&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;904101&quot;,&quot;bccNameRu&quot;:&quot;Поступления отчислений и взносов на обязательное социальное медицинское страхование&quot;,&quot;bccNameKz&quot;:&quot;Міндетті әлеуметтік медициналық сақтандыру аударымдар және  жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;105420&quot;,&quot;bccNameRu&quot;:&quot;Плата за размещ. наружной (визуальной) рекламы на объектах стац. размещ. рекл.в полосе отвода автом.дорог общего польз. обл. знач, за искл. платы за размещ. нар. (виз.) рекл. на объек. стац. размещ. рекл. в пол. отв. автом. ДОП. обл. зн., проход. через терр. гор. рай. зн., сел, поселков, сел. округов&quot;,&quot;bccNameKz&quot;:&quot;Сыртқы (көрнекі) жарнаманы ауд.маң. бар қалалар, ауылдар, кенттер, ауыл. округ. аумақ.арқ. өтетін обл. маң. бар жалпыға ортақ пайдал. автом. жолдар. бөлiнген белдеуiндегі жарн. тұрақты орналастыру объектілерінде орнал. үш. төлемақыны қосп., сырт.(көрн.) жарн. обл. маң. бар ЖОП автом. жолд. бөл. белд. жарн. тұр. орнал. объек. орнал. үш. төлемақы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;902101&quot;,&quot;bccNameRu&quot;:&quot;Поступления социальных отчислений&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтiк аударымдар&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;103101&quot;,&quot;bccNameRu&quot;:&quot;Социальный налог&quot;,&quot;bccNameKz&quot;:&quot;Әлеуметтік салық&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;901101&quot;,&quot;bccNameRu&quot;:&quot;Поступления в пенсионный фонд 901101&quot;,&quot;bccNameKz&quot;:&quot;Міндетті зейнетақы жарналары&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101111&quot;,&quot;bccNameRu&quot;:&quot;Корпоративный подоходный налог с юридических лиц, за исключением поступлений от субъектов крупного предпринимательства и организаций нефтяного сектора&quot;,&quot;bccNameKz&quot;:&quot;Ірі кәсіпкерлік субъектілерінен және мұнай секторы ұйымдарынан түсетін түсімдерді қоспағанда, заңды тұлғалардан алынатын корпоративтік табыс салығы&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0},{&quot;bcc&quot;:&quot;903101&quot;,&quot;bccNameRu&quot;:&quot;Поступления обязательных пенсионных профессиональных взносов&quot;,&quot;bccNameKz&quot;:&quot;Міндетті кәсіптік зейнетақы жарналарының түсімдері&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]},{&quot;nameRu&quot;:&quot;Товарищество с ограниченной ответственностью \\&quot; Юридическая фирма \\&quot;GRATA\\&quot;&quot;,&quot;nameKk&quot;:&quot;\\&quot;GRATA\\&quot; Заң фирма\\&quot; Жауапкершілігі шектеулі серіктестік&quot;,&quot;iinBin&quot;:&quot;920440000372&quot;,&quot;totalArrear&quot;:0,&quot;bccArrearsInfo&quot;:[{&quot;bcc&quot;:&quot;101201&quot;,&quot;bccNameRu&quot;:&quot;Индивидуальный подоходный налог с доходов, облагаемых у источника выплаты.&quot;,&quot;bccNameKz&quot;:&quot;Төлем көзінен салық салынатын табыстардан ұсталатын жеке табыс салығы.&quot;,&quot;taxArrear&quot;:0,&quot;poenaArrear&quot;:0,&quot;percentArrear&quot;:0,&quot;fineArrear&quot;:0,&quot;totalArrear&quot;:0}]}]}]}">\n' +
	'            <button class="btn btn-index" type="submit" id="btnDownload">Скачать</button>\n' +
	'        </form>\n' +
	'\n' +
	'\n' +
	'    </div>\n' +
	'</div>\n' +
	'\n' +
	'\n' +
	'<script>\n' +
	"    var pdfData = '';\n" +
	'\n' +
	'    $(document).ready(function () {\n' +
	'        initCaptcha();\n' +
	'        translateCommonApp();\n' +
	'        emptyApplicationErrors();\n' +
	'\n' +
	"        $(\"#downloadForm\").attr('action', '/apps/services/culs-taxarrear-search-web/rest/pdf?lang=' + lang);\n" +
	'        $("#iinBinLabelId").html(translate(\'index.label\'));\n' +
	'        $("#btnDownload").html(translate(\'result.button.download\'));\n' +
	'    });\n' +
	'\n' +
	'    $("#btnClearField").click(function () {\n' +
	'        $("#iinBin").val(\'\');\n' +
	'        emptyApplicationErrors();\n' +
	'        reloadCaptcha();\n' +
	'    });\n' +
	'</script></div>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'            </div>\n' +
	'          </section>\n' +
	'      </div>\n' +
	'      <div id="rightServ" class="col-md-3">\n' +
	'        <div id="tabFront" class="tab-container hide">\n' +
	'          <!-- Nav tabs -->\n' +
	'          <ul class="nav nav-tabs" role="tablist">\n' +
	'            <li role="presentation" class="active"><a href="#eservice" aria-controls="eservice" role="tab" data-toggle="tab">Электронные сервисы</a></li>\n' +
	'            <li role="presentation"><a href="#ires" aria-controls="ires" role="tab" data-toggle="tab">Информационные ресурсы</a></li>\n' +
	'          </ul>\n' +
	'\n' +
	'          <!-- Tab panes -->\n' +
	'          <div class="tab-content">\n' +
	'            <div role="tabpanel" class="tab-pane fade in active" id="eservice">\n' +
	'                <div>\n' +
	'                  <div class="base">\n' +
	'                    <ul class="base-row">\n' +
	'                      <li><div><a href="/ru/services/taxpayer_search" class="pull-left m-r w-full" title="Поиск налогоплательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/poisk_np.png?itok=deLcj_q2" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Поиск налогоплательщиков</span></span></a></div></li><li><div><a href="http://kgd.gov.kz/ru/app/culs-taxarrear-upcoming-payments-web" class="pull-left m-r w-full" title="Предстоящие платежи для  физических лиц"><span class="simg"><img src="/sites/all/themes/KGD17/images/def-ico.png" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Предстоящие платежи для  физических лиц</span></span></a></div></li><li><div><a href="/ru/services/online-reservation" class="pull-left m-r w-full" title="Онлайн  бронирование"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/123_5.png?itok=NkGpof3c" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Онлайн  бронирование</span></span></a></div></li><li><div><a href="/ru/app/culs-taxarrear-search-web" class="pull-left m-r w-full" title="Сведения об отсутствии (наличии) налоговой задолженности"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/svedeniya_ob_otsutsvii_zadolzhennosti_2.png?itok=r5vZIM8y" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Сведения об отсутствии (наличии) налоговой задолженности</span></span></a></div></li><li><div><a href="/ru/services/taxpayer_search_unreliable" class="pull-left m-r w-full" title="Поиск неблагонадежных налогоплательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/poisk_neblagonadezhnogo_np.png?itok=OD6nv0NG" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Поиск неблагонадежных налогоплательщиков</span></span></a></div></li>                      <li><div class="pull-right m-t-sm m-r-sm"><span><a href="/ru/all/services" class="btn btn-boss pull-right">Все сервисы</a></span></div></li>\n' +
	'                    </ul>\n' +
	'                  </div>\n' +
	'                </div>\n' +
	'            </div>\n' +
	'            <div role="tabpanel" class="tab-pane fade" id="ires">\n' +
	'                <div>\n' +
	'                  <div class="base">\n' +
	'                    <ul class="base-row">\n' +
	'                      <li><div><a href="/ru/content/spravochniki-0" class="pull-left m-r w-full" title="Справочники"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spravochniki_0.png?itok=AIZOTwSu" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Справочники</span></span></a></div></li><li><div><a href="/ru/nsi" class="pull-left m-r w-full" title="Справочники для ВЭД"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spravochniki-dlya-ved.png?itok=u8l_fKCe" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Справочники для ВЭД</span></span></a></div></li><li><div><a href="/ru/section/spiski-platelshchikov-bankrotov" class="pull-left m-r w-full" title="Список плательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spisok-platelshchikov.png?itok=CPj38yu2" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Список плательщиков</span></span></a></div></li><li><div><a href="/ru/section/formy-nalogovoy-otchetnosti" class="pull-left m-r w-full" title="Формы налоговой отчетности"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/formy_nalogovoy_otchetnosti.png?itok=RLEAp4Mf" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Формы налоговой отчетности</span></span></a></div></li><li><div><a href="/ru/content/telefony-doveriya-1" class="pull-left m-r w-full" title="Телефоны доверия"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/telefon_doveriya.png?itok=7OYpxa1i" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Телефоны доверия</span></span></a></div></li>                      <li><div class="pull-right m-t-sm m-r-sm"><span><a href="/ru/all/resources" class="btn btn-boss pull-right">Все сервисы</a></span></div></li>\n' +
	'                    </ul>\n' +
	'                  </div>\n' +
	'                </div>\n' +
	'            </div>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'\n' +
	'        <div id="tabPage" class="row ">\n' +
	'          <div class="col-md-12 ">\n' +
	'            <div class="panel panel-boss">\n' +
	'              <div class="panel-body">\n' +
	'                <div class="block clearfix">\n' +
	'                  <h2 class="text-center text-boss">Электронные сервисы</h2>\n' +
	'                    <div class="left_service">\n' +
	'                      <div class="base">\n' +
	'                        <ul class="base-row">\n' +
	'                          <li><div><a href="/ru/services/taxpayer_search" class="pull-left m-r w-full" title="Поиск налогоплательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/poisk_np.png?itok=deLcj_q2" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Поиск налогоплательщиков</span></span></a></div></li><li><div><a href="http://kgd.gov.kz/ru/app/culs-taxarrear-upcoming-payments-web" class="pull-left m-r w-full" title="Предстоящие платежи для  физических лиц"><span class="simg"><img src="/sites/all/themes/KGD17/images/def-ico.png" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Предстоящие платежи для  физических лиц</span></span></a></div></li><li><div><a href="/ru/services/online-reservation" class="pull-left m-r w-full" title="Онлайн  бронирование"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/123_5.png?itok=NkGpof3c" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Онлайн  бронирование</span></span></a></div></li><li><div><a href="/ru/app/culs-taxarrear-search-web" class="pull-left m-r w-full" title="Сведения об отсутствии (наличии) налоговой задолженности"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/svedeniya_ob_otsutsvii_zadolzhennosti_2.png?itok=r5vZIM8y" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Сведения об отсутствии (наличии) налоговой задолженности</span></span></a></div></li><li><div><a href="/ru/services/taxpayer_search_unreliable" class="pull-left m-r w-full" title="Поиск неблагонадежных налогоплательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/poisk_neblagonadezhnogo_np.png?itok=OD6nv0NG" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Поиск неблагонадежных налогоплательщиков</span></span></a></div></li>                          <li><div class="pull-right m-t-sm m-r-sm"><span><a href="/ru/all/services" class="btn btn-boss pull-right">Все сервисы</a></span></div></li>\n' +
	'                        </ul>\n' +
	'                      </div>\n' +
	'                    </div>\n' +
	'                </div>\n' +
	'              </div>\n' +
	'            </div>\n' +
	'          </div>\n' +
	'          <div class="col-md-12 ">\n' +
	'            <div class="panel panel-boss">\n' +
	'              <div class="panel-body">\n' +
	'                <div class="block clearfix">\n' +
	'                  <h2 class="text-center text-boss">Информационные ресурсы</h2>\n' +
	'                    <div class="left_service">\n' +
	'                      <div class="base">\n' +
	'                        <ul class="base-row">\n' +
	'                          <li><div><a href="/ru/content/spravochniki-0" class="pull-left m-r w-full" title="Справочники"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spravochniki_0.png?itok=AIZOTwSu" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Справочники</span></span></a></div></li><li><div><a href="/ru/nsi" class="pull-left m-r w-full" title="Справочники для ВЭД"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spravochniki-dlya-ved.png?itok=u8l_fKCe" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Справочники для ВЭД</span></span></a></div></li><li><div><a href="/ru/section/spiski-platelshchikov-bankrotov" class="pull-left m-r w-full" title="Список плательщиков"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/spisok-platelshchikov.png?itok=CPj38yu2" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Список плательщиков</span></span></a></div></li><li><div><a href="/ru/section/formy-nalogovoy-otchetnosti" class="pull-left m-r w-full" title="Формы налоговой отчетности"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/formy_nalogovoy_otchetnosti.png?itok=RLEAp4Mf" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Формы налоговой отчетности</span></span></a></div></li><li><div><a href="/ru/content/telefony-doveriya-1" class="pull-left m-r w-full" title="Телефоны доверия"><span class="simg"><img src="http://kgd.gov.kz/sites/default/files/styles/menu-ico/public/telefon_doveriya.png?itok=7OYpxa1i" class="pull-left m-r-sm"></span><span class="clear m-l-sm"><span class="">Телефоны доверия</span></span></a></div></li>                          <li><div class="pull-right m-t-sm m-r-sm"><span><a href="/ru/all/resources" class="btn btn-boss pull-right">Все сервисы</a></span></div></li>\n' +
	'                        </ul>\n' +
	'                      </div>\n' +
	'                    </div>\n' +
	'                </div>\n' +
	'              </div>\n' +
	'            </div>\n' +
	'          </div>\n' +
	'          <div class="col-md-12">\n' +
	'          </div>\n' +
	'        </div>\n' +
	'\n' +
	'        <div class="row">\n' +
	'          <div id="blockCalen" class="col-md-12">\n' +
	'                        <div class="col-md-12 no-padder">\n' +
	'              <div class="region region-footer-third">\n' +
	'  <div id="block-cctags-2" class="block block-cctags">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <canvas width="277px" height="300" id="myCanvas" class="cctags cctags-block"><li><a href="/ru/tagru/vto" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">ВТО</a>&nbsp; </li><li><a href="/ru/tagru/legalizaciya" class="cctags cctags-block vid-9 level-6 depth-0 count-2 ccfilter tooltip" title="" rel="tag">Легализация</a>&nbsp; </li><li><a href="/ru/tagru/perechen-mest-tamozhennogo-oformleniya" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">Перечень мест таможенного оформления</a>&nbsp; </li><li><a href="/ru/tagru/perechen-yuridicheskih-lic-nerezidentov" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">Перечень юридических лиц-нерезидентов</a>&nbsp; </li><li><a href="/ru/tagru/reestry-obektov-intellektualnoy-sobstvennosti" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">Реестры объектов интеллектуальной собственности</a>&nbsp; </li><li><a href="/ru/tagru/shst" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">СХСТ</a>&nbsp; </li><li><a href="/ru/tagru/spiski-nesostoyatelnyh-dolzhnikov" class="cctags cctags-block vid-9 level-6 depth-0 count-2 ccfilter tooltip" title="" rel="tag">Списки несостоятельных должников</a>&nbsp; </li><li><a href="/ru/tagru/cena-syroy-nefti" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">Цена сырой нефти</a>&nbsp; </li><li><a href="/ru/tagru/esf" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">ЭСФ</a>&nbsp; </li><li><a href="/ru/tagru/obrazovavshih" class="cctags cctags-block vid-9 level-1 depth-0 count-1 ccfilter tooltip" title="" rel="tag">образовавших</a>&nbsp; </li></canvas>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'            </div>\n' +
	'                      </div>\n' +
	'        </div>  \n' +
	'                  <div class="container m-b-lg m-t-md">\n' +
	'            <div class="col-md-12"><div class="region region-sidebar-first">\n' +
	'  <div id="block-views-bn-block-1" class="block block-views block-links">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <div class="view view-bn view-id-bn view-display-id-block_1 view-dom-id-4e30a1bde3f77d95799f62465145e15d">\n' +
	'        \n' +
	'  \n' +
	'  \n' +
	'      <div class="view-content">\n' +
	'        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">\n' +
	'    <article id="node-176" class="node node-link node-promoted">\n' +
	'  <div class="node-inner">\n' +
	'    <div class="content">\n' +
	'      \n' +
	'<div class="row list-links field-items">\n' +
	'<p>&nbsp;</p>\n' +
	'      <div class="col one-fourth center field-item even">\n' +
	'      <a target="_blank" href="http://strategy2050.kz/ru/news/category/170/"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/logo/logo_25_kz_-_kopiya_2.jpg" width="44" height="44" alt="http://strategy2050.kz/ru/news/category/170/" title="25-летие Независимости "> 25-летие Независимости </a>\n' +
	'    </div>\n' +
	'      <div class="col one-fourth center field-item odd">\n' +
	'      <a target="_blank" href="http://strategy2050.kz/ru/"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/logo/kz.png" width="61" height="44" alt="http://strategy2050.kz/ru/" title="Казахстан 2050"> Казахстан 2050</a>\n' +
	'    </div>\n' +
	'      <div class="col one-fourth center field-item even">\n' +
	'      <a target="_blank" href="http://www.akorda.kz/ru/state_symbols/about_state_symbols"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/logo/state-symbols.png" width="44" height="44" alt="http://www.akorda.kz/ru/state_symbols/about_state_symbols" title="Государственные символы"> Государственные символы</a>\n' +
	'    </div>\n' +
	'      <div class="col one-fourth center field-item odd">\n' +
	'      <a target="_blank" href="http://kgd.gov.kz/sites/default/files/Poslaniya/poslanie_prezidenta_respubliki_kazahstan_11.11.14.doc"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/logo/nurly-gol.png" width="44" height="44" alt="http://kgd.gov.kz/sites/default/files/Poslaniya/poslanie_prezidenta_respubliki_kazahstan_11.11.14.doc" title="Нұрлы Жол – путь в будущее"> Нұрлы Жол – путь в будущее</a>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'    </div>\n' +
	'          <footer>\n' +
	'      \n' +
	'              <div class="links"></div>\n' +
	'            </footer>\n' +
	'      </div> <!-- /node-inner -->\n' +
	'</article> <!-- /node-->\n' +
	'  </div>\n' +
	'    </div>\n' +
	'  \n' +
	'  \n' +
	'  \n' +
	'  \n' +
	'  \n' +
	'  \n' +
	'</div>  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'</div>\n' +
	'          </div>\n' +
	'              </div>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'\n' +
	'    <div id="BannerFooter" class="container m-b-lg m-t-md">\n' +
	'    <div class="row">\n' +
	'      <div class="row-height">\n' +
	'        <div class="col-sm-1 col-sm-height no-padder text-center">\n' +
	'          <div class="left-inside inside-full-height">\n' +
	'            <a id="prev" class="btn btn-rounded btn-icon btn-md btn-arrow m-t-lg hidden-xs hidden-sm"><i class="fa fa-angle-left fa-2x font-bold"></i></a>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'        <div class="col-sm-10 no-padder">\n' +
	'          <div id="owl-banner" class="owl-carousel owl-theme" style="opacity: 1; display: block;">\n' +
	'            <div class="owl-wrapper-outer"><div class="owl-wrapper" style="width: 5000px; left: 0px; display: block; transition: all 800ms ease 0s; transform: translate3d(-375px, 0px, 0px);"><div class="owl-item" style="width: 125px;"><div class="item"><a href="https://www.qazlatyn.kz/converter/text" title="Qazlatyn.kz" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/qazlatyn.jpg?itok=kMBFqzXr" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="https://open.egov.kz" title="«Открытое правительство»" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/1.jpg?itok=ZNI4HJvQ" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="" title="" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/22.jpg?itok=oZjyCggR" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="" title="" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/bezymyannyy.jpg?itok=cH4f8wwa" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="https://www.enpf.kz/ru/" title="АО «Единый накопительный пенсионный фонд»" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/logo_rus.png?itok=gcILHzUl" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://kgd.gov.kz/ru/section/modernizaciya-30" title="Модернизация 3.0" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/modernizaciya_3.0_log_rus.png?itok=noqpSFog" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://e-auction.gosreestr.kz/p/ru/auction-guest-list" title="Реализация имущества  в счет задолженности по налогам и таможенным платежам посредством электронного аукциона" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/banner_ea_ru.png?itok=fF9soyaX" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://qamqor.gov.kz/portal/page/portal/POPageGroup/Services/Su" title="Мобильное приложение для предпринимателей" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/180x100.png?itok=2pTb9G2K" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://www.akorda.kz/ru" title="Официальный сайт Президента Республики Казахстан " target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/akorda_kz.png?itok=CBC5Xaxb" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://cabinet.salyk.kz/sonowebinfo/" title="Кабинет налогоплательщика" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/banner-rus.png?itok=Gerg7jM-" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://kgd.gov.kz/ru/section/monitoring-inostrannogo-finansirovaniya" title="Мониторинг иностранного финансирования" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/monitoring_inost_finansir_rus_-_kopiya.jpg?itok=ZqIVU6G8" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://kgd.gov.kz/ru/section/o-realizacii-pyati-reform-100-shagov" title="100 конкретных шагов" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/100_shagov11.jpg?itok=aHeHib0m" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://egov.kz" title="Электронное правительство Республики Казахстан" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/egov_0.png?itok=HCvUvrap" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://strategy2050.kz/ru/" title="Стратегия Казахстан 2050" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/el_0_0.png?itok=laTkM-ua" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://www.primeminister.kz/" title="Официальный сайт Премьер-Министра РК" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/banner_rus.jpg?itok=FTTEDhuh" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://prokuror.gov.kz" title="Генеральная прокуратура Республики Казахстан" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/gp_0_1.png?itok=B6XJofs7" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://anticorruption.gov.kz/rus/" title="Агентство Республики Казахстан по делам государственной службы" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/banner_anticorruption_rus.jpg?itok=Dqn67Wwj" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://palata.kz/ru/" title="Национальная палата предпринимателей РК «Атамекен»" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/npp_1.png?itok=05mBtQzB" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://mfa.gov.kz/index.php/ru/component/content/article/12-material-orys/6133-kazakhstan-strana-velikoj-stepi" title="Казахстан - Страна Великой Степи" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/mid_app_ru.jpg?itok=GQr7TxDg" width="180" height="100"></a></div></div><div class="owl-item" style="width: 125px;"><div class="item"><a href="http://www.ank.kz/ " title="Ассоциация налогоплательщиков Казахстана" target="_blank"><img typeof="foaf:Image" src="http://kgd.gov.kz/sites/default/files/styles/bn/public/images/kgd.jpg?itok=jy8xBzFw" width="180" height="100"></a></div></div></div></div>          </div>\n' +
	'        </div>\n' +
	'        <div class="col-sm-1 col-sm-height no-padder text-center">\n' +
	'          <div class="left-inside inside-full-height">\n' +
	'            <a id="next" class="btn btn-rounded btn-icon btn-md btn-arrow m-t-lg hidden-xs hidden-sm"><i class="fa fa-angle-right fa-2x font-bold m-l-xs"></i></a>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'  <div id="akordaWidjet" class="container m-b-lg m-t-md" style="height: 300px;">\n' +
	'      <iframe id="akordaId" src="http://akorda.kz/ru/widget" style="height: 100%; width:100%" frameborder="0"></iframe>  </div>\n' +
	'  <footer id="colophon" class="site-footer" role="contentinfo">\n' +
	'    <div class="container">\n' +
	'      <div class="row">\n' +
	'        <div class="fcred col-sm-9">\n' +
	'          <div class="text-up text-smm">Комитет государственных доходов Министерства финансов Республики Казахстан</div>\n' +
	'          <div class="row m-b m-t">\n' +
	'            <div id="pin" class="col-md-1 col-xs-3 text-right show">\n' +
	'              <a class="text-index" target="_blank" href="https://www.google.kz/maps/place/%D0%9A%D0%BE%D0%BC%D0%B8%D1%82%D0%B5%D1%82+%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D1%85+%D0%B4%D0%BE%D1%85%D0%BE%D0%B4%D0%BE%D0%B2/@51.1682963,71.4204455,18z/data=!3m1!4b1!4m2!3m1!1s0x424586d43657c2ef:0x968deef8f59be185?hl=ru">\n' +
	'                <span class="pull-left ico-pin"></span>\n' +
	'              </a>\n' +
	'            </div>\n' +
	'            <div id="pinText" class="col-md-10 col-xs-9 m-t">\n' +
	'              010000, г. Нур-Султан, пр. Женис, 11              <br>\n' +
	'              тел. канцелярии 8 (7172) 71 84 63, 70 98 83, 70 98 59, 70 20 97            </div>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'        <div class="col-sm-3">\n' +
	'          <div id="downMenu" class="fcred links text-up text-right cuprum text-xs">\n' +
	'            <div class="m-b-xs"><a href="/ru/forum">Форум</a></div>\n' +
	'            <div class="m-b-xs"><a href="/ru/feedback/nojs">Обратная связь</a></div>\n' +
	'            <div class="m-b-xs"><a href="/rss.xml">RSS</a></div>\n' +
	'            <div><a href="/ru/user/0/newsletter" title="Чтобы подписаться на рассылку необходимо зарегистрироваться на сайте">Подписка</a></div>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'      <div class="row">\n' +
	'        <div class="col-md-8 hidden-xs hidden-sm"><div class="text-xs">© 2020 Комитет государственных доходов Министерства финансов Республики Казахстан</div></div>\n' +
	'        <div class="col-md-4">\n' +
	'          <div class="pull-right">\n' +
	'                          <div class="region region-counter">\n' +
	'  <div id="block-block-10" class="block block-block">\n' +
	'\n' +
	'      \n' +
	'  <div class="content">\n' +
	'    <!-- Yandex.Metrika informer --><a href="https://metrika.yandex.ru/stat/?id=34410855&amp;from=informer" target="_blank" rel="nofollow"><img src="https://informer.yandex.ru/informer/34410855/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"></a><!-- /Yandex.Metrika informer --> <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter34410855 = new Ya.Metrika({ id:34410855, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="https://mc.yandex.ru/watch/34410855" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->\n' +
	'\n' +
	'<!--Openstat-->\n' +
	'<span id="openstat1"><a target="_blank" href="http://rating.openstat.ru/?cid=1"><img alt="Openstat" border="0" src="http://openstat.net/i/89.gif?tc=458efc"></a></span>\n' +
	'<script type="text/javascript">\n' +
	'var openstat = { counter: 1, image: 89, color: "458efc", next: openstat };\n' +
	'(function(d, t, p) {\n' +
	'var j = d.createElement(t); j.async = true; j.type = "text/javascript";\n' +
	'j.src = ("https:" == p ? "https:" : "http:") + "//openstat.net/cnt.js";\n' +
	'var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);\n' +
	'})(document, "script", document.location.protocol);\n' +
	'</script>\n' +
	'<!--/Openstat-->\n' +
	'\n' +
	'<!-- HotLog -->\n' +
	'<span id="hotlog_counter"><a href="http://click.hotlog.ru/?2481088" target="_blank"><img src="http://hit34.hotlog.ru/cgi-bin/hotlog/count?0.442380869606275&amp;s=2481088&amp;im=353&amp;r=&amp;pg=http%3A//kgd.gov.kz/ru/app/culs-taxarrear-search-web&amp;j=N&amp;wh=800x600&amp;px=24&amp;cver=1&amp;js=1.3" class="hotlog_counter" border="0" alt="HotLog" title="HotLog" width="88" height="31"></a></span>\n' +
	'<span id="hotlog_dyn"><script type="text/javascript" async="" src="http://js.hotlog.ru/dcounter/2481088.js"></script></span>\n' +
	'<script type="text/javascript"> var hot_s = document.createElement(\'script\');\n' +
	"hot_s.type = 'text/javascript'; hot_s.async = true;\n" +
	"hot_s.src = 'http://js.hotlog.ru/dcounter/2481088.js';\n" +
	"hot_d = document.getElementById('hotlog_dyn');\n" +
	'hot_d.appendChild(hot_s);\n' +
	'</script>\n' +
	'<noscript>\n' +
	'<a href="http://click.hotlog.ru/?2481088" target="_blank">\n' +
	'<img src="http://hit34.hotlog.ru/cgi-bin/hotlog/count?s=2481088&im=353" border="0" \n' +
	'title="HotLog" alt="HotLog"></a>\n' +
	'</noscript>\n' +
	'<!-- /HotLog -->\n' +
	'\n' +
	'<!-- ZERO.kz -->\n' +
	'<span id="_zero_72460"><a href="http://zero.kz/?s=72460" target="_blank"><img border="0px" width="88" height="31" alt="ZERO.KZ" src="http://c.zero.kz/z.png?u=72460&amp;t=1&amp;cc=z5eb7e6fccd86a&amp;s=5eb7e6fd8d59d&amp;sh=1&amp;slt=0&amp;d=0&amp;wd=800&amp;hg=600&amp;cd=24&amp;w=800&amp;h=600&amp;ln=en-us&amp;je=0&amp;cs=UTF-8&amp;ce=1&amp;du=http%3A%2F%2Fkgd.gov.kz%2Fru%2Fapp%2Fculs-taxarrear-search-web&amp;tz=0&amp;dt=%D0%A1%D0%B2%D0%B5%D0%B4%D0%B5%D0%BD%D0%B8%D1%8F%20%D0%BE%D0%B1%20%D0%BE%D1%82%D1%81%D1%83%D1%82%D1%81%D1%82%D0%B2%D0%B8%D0%B8%20(%D0%BD%D0%B0%D0%BB%D0%B8%D1%87%D0%B8%D0%B8)%20%D0%B7%D0%B0%D0%B4%D0%BE%D0%BB%D0%B6%D0%B5%D0%BD%D0%BD%D0%BE%D1%81%D1%82%D0%B8%2C%20%D1%83%D1%87%D0%B5%D1%82%20%D0%BF%D0%BE%20%D0%BA%D0%BE%D1%82%D0%BE%D1%80%D1%8B%D0%BC%20%D0%B2%D0%B5%D0%B4%D0%B5%D1%82%D1%81%D1%8F%20%D0%B2%20%D0%BE%D1%80%D0%B3%D0%B0%D0%BD%D0%B0%D1%85%20%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D1%85%20%D0%B4%D0%BE%D1%85%D0%BE%D0%B4%D0%BE%D0%B2%20%7C%20%D0%9A%D0%BE%D0%BC%D0%B8%D1%82%D0%B5%D1%82%20%D0%B3%D0%BE%D1%81%D1%83%D0%B4%D0%B0%D1%80%D1%81%D1%82%D0%B2%D0%B5%D0%BD%D0%BD%D1%8B%D1%85%20%D0%B4%D0%BE%D1%85%D0%BE%D0%B4%D0%BE%D0%B2%20%D0%9C%D0%B8%D0%BD%D0%B8%D1%81%D1%82%D0%B5%D1%80%D1%81%D1%82%D0%B2%D0%B0%20%D1%84%D0%B8%D0%BD%D0%B0%D0%BD%D1%81%D0%BE%D0%B2%20%D0%A0%D0%B5%D1%81%D0%BF%D1%83%D0%B1%D0%BB%D0%B8%D0%BA%D0%B8%20%D0%9A%D0%B0%D0%B7%D0%B0%D1%85%D1%81%D1%82%D0%B0%D0%BD&amp;9.306211884158817"></a></span>\n' +
	'\n' +
	'<script type="text/javascript"><!--\n' +
	'  var _zero_kz_ = _zero_kz_ || [];\n' +
	'  _zero_kz_.push(["id", 72460]);\n' +
	'  // Цвет кнопки\n' +
	'  _zero_kz_.push(["type", 1]);\n' +
	'  // Проверять url каждые 200 мс, при изменении перегружать код счётчика\n' +
	'  // _zero_kz_.push(["url_watcher", 200]);\n' +
	'\n' +
	'  (function () {\n' +
	'    var a = document.getElementsByTagName("script")[0],\n' +
	'    s = document.createElement("script");\n' +
	'    s.type = "text/javascript";\n' +
	'    s.async = true;\n' +
	'    s.src = (document.location.protocol == "https:" ? "https:" : "http:")\n' +
	'    + "//c.zero.kz/z.js";\n' +
	'    a.parentNode.insertBefore(s, a);\n' +
	'  })(); //-->\n' +
	'</script>\n' +
	'<!-- End ZERO.kz -->\n' +
	'\n' +
	'  </div>\n' +
	'  \n' +
	'</div> <!-- /.block -->\n' +
	'</div>\n' +
	' <!-- /.region -->\n' +
	'                      </div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'  </footer></div>\n' +
	'</div>\n' +
	'\n' +
	'<div class="login-form dialog" id="loginFormDialog">\n' +
	'  <div class="panel panel-default m-b-none">\n' +
	'    <div class="panel-boss">\n' +
	'      <div class="clearfix">\n' +
	'        <div class="clear">\n' +
	'          <div class="h4 m-t-xs m-b-xs">\n' +
	'            Вход            <i id="closeModal" class="fa fa fa-close pull-right text-sm m-t-xs cursorHand"></i>\n' +
	'          </div>\n' +
	'        </div>\n' +
	'      </div>\n' +
	'    </div>\n' +
	'    <div class="panel m-b-none">\n' +
	'      <div class="panel-body">\n' +
	'                  <div class="region region-login">\n' +
	'  <form method="post" action="/ru/app/culs-taxarrear-search-web">\n' +
	'  <div class="form-group">\n' +
	'    <input type="text" name="name" class="form-control" placeholder="Имя">\n' +
	'  </div>\n' +
	'  <div class="form-group">\n' +
	'    <input type="password" name="pass" class="form-control" placeholder="Пароль">\n' +
	'    <input type="hidden" value="" name="form_build_id">\n' +
	'    <input type="hidden" value="user_login_block" name="form_id">\n' +
	'  </div>\n' +
	'  <div class="form-group">\n' +
	'      </div>  \n' +
	'  <div class="checkbox">\n' +
	'    <a href="/ru/user/password">Забыли пароль?</a>  </div>\n' +
	'  <button type="submit" class="btn btn-sm text-sm col-sm-12 btn-index">Вход</button>\n' +
	'</form></div>\n' +
	' <!-- /.region -->\n' +
	'              </div>\n' +
	'    </div>\n' +
	'  </div>\n' +
	'</div>\n' +
	'      <script type="text/javascript" src="//kgd.gov.kz/sites/default/files/advagg_js/js__BZLMKe8D4O-_ro1OfkhRNUNjdincPH4gFEyJsffG57o__LPDDxBSi-DT9yGRK5cj905DjWkhjUeyliRYy4eIpSho__iFQX4H3jw4qSn9BeERFJlBuZNBASR3Or1rI0CUFuY7Q.js"></script>\n' +
	'    \n' +
	'  \n' +
	'\n' +
	'<div id="cboxOverlay" style="display: none;"></div><div id="colorbox" class="" role="dialog" tabindex="-1" style="display: none;"><div id="cboxWrapper"><div><div id="cboxTopLeft" style="float: left;"></div><div id="cboxTopCenter" style="float: left;"></div><div id="cboxTopRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxMiddleLeft" style="float: left;"></div><div id="cboxContent" style="float: left;"><div id="cboxTitle" style="float: left;"></div><div id="cboxCurrent" style="float: left;"></div><button type="button" id="cboxPrevious"></button><button type="button" id="cboxNext"></button><button id="cboxSlideshow"></button><div id="cboxLoadingOverlay" style="float: left;"></div><div id="cboxLoadingGraphic" style="float: left;"></div></div><div id="cboxMiddleRight" style="float: left;"></div></div><div style="clear: left;"><div id="cboxBottomLeft" style="float: left;"></div><div id="cboxBottomCenter" style="float: left;"></div><div id="cboxBottomRight" style="float: left;"></div></div></div><div style="position: absolute; width: 9999px; visibility: hidden; display: none; max-width: none;"></div></div></body></html>';

module.exports = site_str;
