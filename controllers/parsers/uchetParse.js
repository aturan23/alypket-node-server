const puppeteer = require('puppeteer');
const HTMLParser = require('node-html-parser');

const uchetBodyParse = async (text) => {
  const url = `https://pk.uchet.kz/c/search/?search=${text}`;
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);
  const exist = await page.$('.not-found-anything');
  let data = null;
  if (!exist) {
    const body = await page.content();
    data = parser(body);
  }
  await browser.close();
  return data;
};

const parser = (body) => {
  const root = HTMLParser.parse(body);
  const container = root.querySelectorAll('.company-item.container');
  return container.map((item) => {
    const title = item.querySelector('.company-title').innerHTML;
    const infoTitleList = item.querySelectorAll('.info-title').map((item) => item.innerHTML);
    const infoValueList = item.querySelectorAll('.info-value').map((item) => item.innerHTML);
    const infoList = [];
    let bin = null;
    for (let i = 0; i < infoTitleList.length; i++) {
      if (infoTitleList[i] === 'БИН:') {
        bin = infoValueList[i]
      }
      infoList.push({
        title: infoTitleList[i],
        value: infoValueList[i],
      });
    }
    return {
      title,
      bin,
      infoList,
    };
  });
}

module.exports = uchetBodyParse;
