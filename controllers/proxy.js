var cors_proxy = require('cors-anywhere');
const asyncHandler = require('../middleware/async');

let proxy = cors_proxy.createServer({
  originWhitelist: [], // Allow all origins
  requireHeaders: [], // Do not require any headers.
  removeHeaders: [] // Do not remove any headers.
});

exports.postProxy = asyncHandler(async (req, res, next) => {
  req.url = req.url.replace('/node/proxy/', '/');
  proxy.emit('request', req, res);
});

exports.getProxy = asyncHandler(async (req, res, next) => {
  req.url = req.url.replace('/node/proxy/', '/');
  proxy.emit('request', req, res);
});
