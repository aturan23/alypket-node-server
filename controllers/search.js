const uchetBodyParse = require('./parsers/uchetParse');
const kgdgovBodyParse = require('./parsers/kgdgovParse');
const asyncHandler = require('../middleware/async');
const HTMLParser = require('node-html-parser');

exports.search = asyncHandler(async (req, res, next) => {
  const { text } = req.body;
  const uchetResult = await uchetBodyParse(text);
  // const kgdgovResult = kgdgovBodyParse('');
  res.status(200).json(uchetResult);
});
