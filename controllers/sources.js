const puppeteer = require('puppeteer');
const fs = require('fs');
const asyncHandler = require('../middleware/async');
const captcha = require("async-captcha");
const anticaptcha = new captcha('0b0667ad32f1dc0420a1f6ed55d411b6', 2, 10);

exports.kgdgovLoad = asyncHandler(async (req, res, next) => {
  const url = 'http://kgd.gov.kz/ru/app/culs-taxarrear-search-web';
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(url);
  await page.screenshot({path: 'captcha.png', clip: {x: 50, y: 500, width: 300, height: 100}});
  const captchaCode = await anticaptcha.getResult(base64_encode('captcha.png'));
  await page.type('#iinBin', req.params.text);
  await page.type('#captcha-user-value', captchaCode.toUpperCase());
  await page.click('#btnGetResult');
  await delay(5000);
  const body = await page.content();
  await browser.close();
  res.set('Content-Type', 'text/html');
  res.status(200).send(body);
});

function base64_encode(file) {
  const bitmap = fs.readFileSync(file);
  return new Buffer(bitmap).toString('base64');
}

function delay(time) {
  return new Promise((resolve) => {
    setTimeout(resolve, time)
  });
}
