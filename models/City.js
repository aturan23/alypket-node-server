const mongoose = require('mongoose');

const CitySchema = new mongoose.Schema({
  name: {
    type: String,
    trim: true,
    required: [true, 'Please add a city name']
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('City', CitySchema);
