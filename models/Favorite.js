const mongoose = require('mongoose');

const FavoriteSchema = new mongoose.Schema({
  bootcampId: {
    type: String,
    required: [true, 'Please add a bootcamp id']
  },
  createdAt: {
    type: Date,
    default: Date.now
  },
});

module.exports = mongoose.model('Favorite', FavoriteSchema);
