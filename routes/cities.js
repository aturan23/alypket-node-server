const express = require('express');
const {
  getCities,
  getCity,
  createCity,
  updateCity,
  deleteCity,
} = require('../controllers/city');

const City = require('../models/City');

const router = express.Router();

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');

router
  .route('/')
  .get(advancedResults(City, 'cities'), getCities)
  .post(protect, authorize('publisher', 'admin'), createCity);

router
  .route('/:id')
  .get(getCity)
  .put(protect, authorize('publisher', 'admin'), updateCity)
  .delete(protect, authorize('publisher', 'admin'), deleteCity);

module.exports = router;
