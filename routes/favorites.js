const express = require('express');
const {
  getFavorites,
  createFavorite,
  deleteFavorite,
} = require('../controllers/favorites');
const advancedResults = require('../middleware/advancedResults');
const Favorite = require('../models/Favorite');

const router = express.Router();

router
  .route('/')
  .get(advancedResults(Favorite, 'favorites'), getFavorites)
  .post(createFavorite)

router
  .route('/:id')
  .delete(deleteFavorite);

module.exports = router;
