const express = require('express');
const {
  loadPage,
} = require('../controllers/pageLoader');

const router = express.Router();

router.route('/url/:siteUrl*').get(loadPage);

module.exports = router;
