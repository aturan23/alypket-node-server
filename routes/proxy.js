const express = require('express');
const {
  getProxy,
  postProxy,
} = require('../controllers/proxy');

const router = express.Router();

router.route('/:proxyUrl*').get(getProxy).post(postProxy);

module.exports = router;
