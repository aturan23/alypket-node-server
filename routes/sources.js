const express = require('express');
const {
  kgdgovLoad
} = require('../controllers/sources');

const router = express.Router();

router.route('/kgdgov/:text*').get(kgdgovLoad);

module.exports = router;
