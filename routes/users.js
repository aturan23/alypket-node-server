const express = require('express');
const {
  getUsers,
  getUser,
  createUser,
  updateUser,
  deleteUser,
  userPhotoUpload,
  getUserNotifications,
  updateUserNotifications,
  getUserFavorites,
  updateUserFavorites
} = require('../controllers/users');

const User = require('../models/User');

const router = express.Router({ mergeParams: true });

const advancedResults = require('../middleware/advancedResults');
const { protect, authorize } = require('../middleware/auth');

router.use(protect);
router.use(authorize('admin'));

router
  .route('/:id/photo')
  .put(userPhotoUpload);

router
  .route('/')
  .get(advancedResults(User), getUsers)
  .post(createUser);

router
  .route('/:id')
  .get(getUser)
  .put(updateUser)
  .delete(deleteUser);

router
  .route('/:id/notifications')
  .get(getUserNotifications)
  .put(updateUserNotifications)

router
  .route('/:id/favorites')
  .get(getUserFavorites)
  .put(updateUserFavorites)

module.exports = router;
